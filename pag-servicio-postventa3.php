<?php
/**
 * Template Name: Servicio Post-Venta-test
*/
get_header(); 
get_template_part('secciones/header-normal');

	$current = $post->ID;	
	$parent = $post->post_parent;	
	$grandparent_get = get_post($parent);	
	$grandparent = $grandparent_get->post_parent;

get_template_part("funciones/mysql.class");

$bd = new BD();
$bd->conectar();

?>



<section class="pagina-interior">
<div class="container">

	<ul class="breadcrumb">
		<li>
			<a href="<?php bloginfo('url');?>">Inicio</a>
		</li>
		<?php if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)):?>

		<?php else:?>
		<li>
			<?php echo get_the_title($parent); ?>
		</li>
		<?php endif;?>

		<li class="active">
			<?php the_title();?>
		</li>
	</ul>

	<h2 class="text-center"><br> <a href="#" class="av-select-project">Selecciona tu proyecto</a> <hr></h2>
	<!-- Nav tabs -->
	<div class="row av-postventa">
		<div class="col-xs-6 text-left">
		<h2>Departamentos</h2>
		<!-- <img class="av-image-postventa" style="width:100%;" src="https://www.avellaneda.cl/web/wp-content/uploads/2018/11/constructora.jpg" alt=""> -->
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Arcadia</p></a>
			<a class="av-tab-link nombre-landing" href="#constructora" aria-controls="constructora" role="tab" data-toggle="tab"><p>Vista Portezuelo</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Parque Mirador</p></a>
		</div>
		<div class="col-xs-6 text-left">
		<h2>Casas</h2>
			<!-- <img class="av-image-postventa" style="width:100%;" src="https://www.avellaneda.cl/web/wp-content/uploads/2018/11/inmobiliaria.jpg" alt=""> -->
			<a class="av-tab-link nombre-landing" href="#constructora" aria-controls="constructora" role="tab" data-toggle="tab"><p>Alto Casares</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Don Manuel</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Itahue</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Laguna Norte (Entrega Hasta Diciembre 2016)</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Laguna Norte (Entrega 2017 en Adelante)</p></a>
			<a class="av-tab-link nombre-landing" href="#constructora" aria-controls="constructora" role="tab" data-toggle="tab"><p>Plaza Buin</p></a>
			<a class="av-tab-link nombre-landing" href="#constructora" aria-controls="constructora" role="tab" data-toggle="tab"><p>Parque Buin</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Terrandina</p></a>
			<a class="av-tab-link nombre-landing" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab"><p>Umbral</p></a>
		</div>
	</div>
	<!-- ul class="nav nav-pills nav-justified " role="tablist">
		<li role="presentation" >
			<a class="av-tab-link" href="#constructora" aria-controls="constructora" role="tab" data-toggle="tab">
			
			</a>
		</li>
		<li role="presentation">
			<a class="av-tab-link" href="#inmobiliaria" aria-controls="inmobiliaria" role="tab" data-toggle="tab">
				
				
				
			</a>
		</li>
	</ul> -->

	<div class="tab-content av-tab-postventa">

		<div role="tabpanel" class="tab-pane fade in " id="constructora">
			<div class="codegena_iframe_content">
				<iframe class="codegena_iframe" src="https://www.nubix.site/avellaneda/postventa/formulariopv/" frameborder="0" style="background:url('//codegena.com/wp-content/uploads/2015/09/loading.gif') white center center no-repeat;border:0px;">
				</iframe>
			</div>
			<style>
				.nav-pills>li>a{
					padding:30px;
					display: table;
				}
				.codegena_iframe iframe {
					overflow:hidden;
				} 
				/* .codegena_iframe {
					position: relative;
					overflow: hidden;
					padding-top: 56.25%;
				}

				 .tab-content>.active{ 
						min-height:950px;
					}
				@media (max-width: 200px){
					.tab-content>.active{ 
						min-height:950px;
					}
				} */
			</style>
		</div>

		<div role="tabpanel" class="tab-pane fade" id="inmobiliaria">
		<br>
			<h1 class="display">
				<?php the_title();?>
			</h1>
			<div class="row bs-wizard" style="border-bottom:0;">

				<div class="col-xs-4 bs-wizard-step paso-1  complete ">
					<div class="text-center bs-wizard-stepnum">Paso 1</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Datos del Propietario</div>
				</div>

				<div class="col-xs-4 bs-wizard-step paso-2 disabled">
					<div class="text-center bs-wizard-stepnum">Paso 2</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Datos del Solicitante</div>
				</div>
				<div class="col-xs-4 bs-wizard-step paso-3 disabled">
					<div class="text-center bs-wizard-stepnum">Paso 3</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Datos de la Vivienda</div>
				</div>

			</div><!-- .ROW -->

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div id="gracias-form-postventa">
				<div class="col-md-12">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado2.svg" class="img-responsive icon-enviado">
					<h3>Gracias <span class="nombre-persona"></span></h3>
					<p><strong>Hemos recibido su solicitud de postventa con N° <span class="numero-solicitud"></span></strong></p>
					<p>Nos pondremos en contacto con usted en un plazo máximo de 5 días hábiles, para coordinar una visita.</p>
					<hr>
					<strong>
						<p>Horario de Visita</p>
					</strong>
					<p>Lunes a Viernes de 9:00 a 17:00</p>
					<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						Ante emergencias de los servicios básicos (gas, agua, luz) debe contactarse directamente con la empresa
						proveedora.</div>
				</div>
			</div>

			<select name="proyecto_db" id="proyecto_db" tabindex="14" class="required hide">
				<option value="" disabled="disabled" selected class="required">Seleccione proyecto</option>
				<?php
				$bd2 = new BD();
				$bd2->conectar_postventa();
				$bd2->consulta("SELECT * FROM proyectos ORDER BY nombre ASC");
				for($i=0; $i<$bd2->numregistros(); $i++){
					$proyecto = $bd2->fetch_row($i);
				?>
				<option value="<?=$proyecto[" id"]?>" data-email="
					<?=$proyecto["email"]?>" data-cc="
					<?=$proyecto["cc"]?>" data-bcc="
					<?=$proyecto["bcc"]?>">
					<?=$proyecto["nombre"]?>
				</option>


				<?php
				}
				$bd2->desconectar();
				?>
			</select>

			<div id="formulario-postventa">
				<?php the_content();?>
			</div>

			<?php endwhile; else : ?>
			<p>
				<?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
			</p>
			<?php endif; ?>
		</div>
		
	</div><!-- .tab-content-->


	</div><!-- .CONTAINER -->
</section>











<?php get_footer();?>