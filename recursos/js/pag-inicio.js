$(window).on('load', function(){


    var owl_logos = $('.owl-logos');
    owl_logos.owlCarousel({
  		items: 5,
        loop: true,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsive: {
            0: {
                dots: true,
                autoplay: false,
                nav: false,
                items: 2,
            },
            991: {
            	items: 5,
            }

        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });






});