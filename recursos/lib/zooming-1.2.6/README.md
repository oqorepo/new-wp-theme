# Zooming [![npm](https://img.shields.io/npm/v/zooming.svg?style=flat-square)](https://www.npmjs.com/package/zooming)

Image zoom that makes sense. [Demo](http://desmonding.me/zooming/)

- Pure JavaScript & built with mobile in mind.
- Smooth animations with intuitive gestures.
- Zoom into a hi-res image if supplied.
- Easy to integrate & customizable.

## Get Started

Please see [Documentation](http://desmonding.me/zooming/docs) for detailed guide.

## Contributing

Fork and clone it. Under project folder:

```bash
yarn        # or npm install
yarn watch  # or npm run watch
```

## Test

`yarn test`

## Credit

Inspired by [zoom.js](https://github.com/fat/zoom.js) and [zoomerang](https://github.com/yyx990803/zoomerang). First demo image from [Journey](http://thatgamecompany.com/games/journey/). Second demo image [journey](http://www.pixiv.net/member_illust.php?mode=medium&illust_id=36017129) by [飴村](http://www.pixiv.net/member.php?id=47488).
