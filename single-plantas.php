<?php
/**
* Template Name: Proyecto Single Plantas
*/
get_header('plantas');
$vincular = get_field('vincular_planta_a_proyecto');
$noAdd = get_field('ingresa_el_correo_donde_quieres_que_lleguen_las_solicitudes', $vincular->ID);
$plantaremarketing = get_field('fotografia_planta');
$plantaremarketing_array = wp_get_attachment_image_src($plantaremarketing, 'full');
$archivo = get_field('sube_el_ebook_digital', $vincular->ID);
$telefono1 = get_field('telefono', $vincular->ID);
$telefono2 = get_field('telefono_2', $vincular->ID);
$direccion = get_field('direccion', $vincular->ID);
$imagesP = get_field('foto_portada_para_menu', $vincular->ID);
$srcImg = wp_get_attachment_image_src($imagesP, 'full');
$logoP = get_field('logo_proyecto', $vincular->ID);
$logoImg = wp_get_attachment_image_src($logoP, 'full');
// check if the repeater field has rows of data
if( have_rows('slides_principal', $vincular->ID) ):
	// loop through the rows of data
   while ( have_rows('slides_principal', $vincular->ID) ) : the_row();
	   // display a sub field value
	   $images = get_sub_field('fotografia_mobile');
	   $imagesSrc = wp_get_attachment_image_src($images, 'full');
   endwhile;
endif;

?>
<script>
$(document).ready(function(){
var	NombreProyecto = '<?php echo $vincular->post_title;?>';
$('input[name="AceptaMailing"]').val('Si');
$('input[name=NombreProyecto]').val('<?php echo $vincular->post_title;?>');
var noAdd = "<?php echo $noAdd;?>";
$('input[name="EmailDB"]').val(noAdd);

$('input[name="MailingPlantaMetraje"]').val('<?php the_field('superficie_total');?>');
$('input[name="MailingPlantaPrecio"]').val('<?php the_field('precio_marketing', $vincular->ID);?>');
$('input[name="MailingImgPrincipal"]').val('<?php echo $srcImg[0];?>');
$('input[name="MailingImgPiloto"]').val('<?php echo $imagesSrc[0];?>');
$('input[name="MailingImgPlanta"]').val('<?php echo $plantaremarketing_array[0];?>');
$('input[name="MailingTipoPlanta"]').val('<?php the_field('tipo_de_planta');?>');
$('input[name="MailingEmail"]').val('<?php the_field('correo_electronico', $vincular->ID);?>');

$('input[name="MailingSalaVentasDir"]').val('<?php echo $direccion;?>');
$('input[name="MailingSalaVentasTel1"]').val('<?php echo $telefono1;?>');
$('input[name="MailingSalaVentasTel2"]').val('<?php echo $telefono2;?>');
$('input[name="MailingPDF"]').val('<?php echo $archivo;?>');
$('input[name="post-url"]').val(window.location.href);
$('input[name="ProjectName"]').val('<?php echo $vincular->post_title;?>');
$('input[name="ProjectLogo"]').val('<?php echo $logoImg[0];?>');

if ( NombreProyecto === 'Edificio Mirror'){
 $('input[name="ProjectType"]').val('proyecto');
}else{
	$('input[name="ProjectType"]').val('piloto');
}
console.log(NombreProyecto);
});
dataLayer.push({
'tipoPagina': 'Planta',
});
</script>
<div class="menu-plantas solo-celular">
	<a href="<?php bloginfo('url');?>/proyectos/<?php echo $vincular->post_name;?>/#plantas"  >
		<div class="menu-cotizar tooltipster2" title="Volver a <?php echo $vincular->post_title;?>"  data-tooltipster='{"side":"bottom"}'" >
			<div class="botones-menu-fixed">
				<div class="iconos-menu-fixed cerrar">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/cerrar.svg" alt="Ícono Cerrar" class="img-responsive"  />
				</div>
			</div>
		</div>
	</a>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo">
					<a href="<?php bloginfo('url');?>" class="tooltipster"  title="Ir al Inicio" >
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" alt="<?php bloginfo('name');?>" class="img-responsive"></a>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="menu-plantas solo-desktop">
	<a href="<?php bloginfo('url');?>/proyectos/<?php echo $vincular->post_name;?>/#plantas"  >
		<div class="menu-cotizar tooltipster2" title="Volver a <?php echo $vincular->post_title;?>"  data-tooltipster='{"side":"bottom"}'" >
			<div class="botones-menu-fixed">
				<div class="iconos-menu-fixed cerrar">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/cerrar.svg" alt="Ícono Cerrar" class="img-responsive"  />
				</div>
			</div>
		</div>
	</a>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="logo">
					<a href="<?php bloginfo('url');?>" class="tooltipster"  title="Ir al Inicio" >
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" alt="<?php bloginfo('name');?>" class="img-responsive"></a>
				</div>
			</div>
			<div class="col-md-9">
				<ul class="menu-single-plantas">
					<script>
					dataLayer.push({
					'Proyecto': '<?php echo $vincular->post_title;?>',
					});
					</script>
					<li>
						<button class="btn btn-gris-claro pull-right btn-ver-mas-plantas">Ver más plantas <i class="fas fa-angle-down"></i></button>
					</li>
					<li>
						<a href="<?php bloginfo('url');?>/proyectos/<?php echo $vincular->post_name;?>">
							<h2>
							<span class="gris">Proyecto</span>
							<?php echo $vincular->post_title;?>
							</h2>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>



			<section class="col-md-12 solo-celular" style="background:#dddddd; padding-top:70px; padding-bottom:15px;">

					<script>
					dataLayer.push({
					'Proyecto': '<?php echo $vincular->post_title;?>',
					});
					</script>

						<button class="btn btn-block btn-gris-claro pull-right btn-ver-mas-plantas">Ver más plantas <i class="fas fa-angle-down"></i></button>



			</div>


<?php
$post_id = get_the_ID();
$proyectoID = $vincular->ID;
$plantas = new WP_Query(array(
'post_type'      	=> 'plantas',
'posts_per_page'	=> -1,
'post_status'		=> 'publish',

'meta_query' 		=> array(
		'relation' 			=> 'AND',
		array(
	        'key'			=> 'vincular_planta_a_proyecto',
	        'compare'		=> 'IN',
	        'value'			=> $proyectoID,
	    ),
	    array(
			'key' 		=> 'estado',
			'value' 	=> 'Agotado',
			'compare' 	=> 'NOT LIKE'
			)

    ),






'meta_key'			=> 'superficie_total',
'orderby'			=> 'meta_value',
'order'				=> 'ASC',
'post__not_in' => array($post_id),
));
$count = $plantas->post_count;?>
<?php if ( $plantas->have_posts() ) : ?>
<section class="ver-mas-plantas">
	<div class="container">

			<?php	$proyecto = get_the_title();	?>



			<div class="owl-carousel owl-theme owl-ver-mas-plantas">


			<?php while ( $plantas->have_posts() ) : $plantas->the_post();?>

				<?php
				$fotografia_planta = get_field('fotografia_planta');
				$size_planta = 'fotografias-departamentos';
				$image_array_planta = wp_get_attachment_image_src($fotografia_planta, $size_planta);
				$descripcion = get_field('descripcion_breve_pagina_del_proyecto');
				$supertotal = get_field('superficie_total');
				$tienestadocustom = get_field('tiene_estado_personalizado');
				$estadopersonalizado = get_field('estado_personalizado');
				?>
				<a href="<?php the_permalink();?>" title="Ver Planta <?php the_title();?>" class="tooltipster">

					<?php if ($tienestadocustom == "Si"):?>

					<div class="ribbon ribbon-top-left"><span style="font-size:11px;"><?php echo $estadopersonalizado;?></span></div>

					<?php endif;?>

					<div class="text-center">
						<img src="<?php echo $image_array_planta[0];?>" class="img-responsive" alt="Planta <?php the_title();?> - <?php echo $vincular_parent_planta->post_title;?>"/>
						<div class="bloque-mas-plantas">
							<h4><?php the_title();?></h4>
							<div class="descripcion-ver-mas-plantas"><?php echo $descripcion;?>
							<span class="badge badge-secondary"><?php echo $supertotal;?> m²</span>
							</div>
						</div>
					</div>
				</a>
				<?php endwhile;?>
			</div>
			<?php endif;?>
		</div>
</section>
<?php wp_reset_postdata(); ?>




<section class="single-plantas">
<div class="container">
	<div class="contenedor-single-plantas">
		<div class="row">






			<div class="col-md-8 no-padding-right">




				<?php
				// variables
				$pregunta = get_field('pregunta_fotografias');
				$fotografia = get_field('fotografia_planta');
				$size = 'full';
				$image_array = wp_get_attachment_image_src($fotografia, $size);
				$image_url = $image_array[0];
				$estado = get_field('estado');
				$tienestadocustom = get_field('tiene_estado_personalizado');
				$estadopersonalizado = get_field('estado_personalizado');
				?>
				<?php if ($pregunta == "Si"):?>
				<div class="box">
					<?php if ($estado == "Agotado"):?>
					<div class="ribbon ribbon-top-left"><span><?php the_field('estado');?></span></div>
					<?php else:?>
					<?php if ($tienestadocustom == "Si"):?>
					<div class="ribbon ribbon-top-left"><span style="font-size:11px; top:40px;"><?php echo $estadopersonalizado;?></span></div>
					<?php endif;?>
					<?php endif;?>
					<?php if ($fotografia):?>

					<a href="<?php echo $image_url;?>" data-fancybox="planta" data-caption="<h4><?php echo $vincular->post_title;?></h4>Planta: <?php the_title();?> ">

						<div class="solo-celular">
							<img src="<?php echo $image_url;?>" class="foto-planta-grande img-responsive tooltipster" title="Ampliar" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>" />
						</div>
						<div class="solo-desktop">

							<img src="<?php echo $image_url;?>" class="foto-planta-grande img-responsive tooltipster" title="Ampliar" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>" />
						</div>
					</a>

					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto3.jpg" alt="Sin Fotografía" class="img-responsive" />
					<?php endif;?>
				</div>
				<div class="ayuda"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</div>
				<?php else:?>
				<?php if( have_rows('fotografias_plantas_loop') ):
				$count = 0;
				$conteo = 0;
				$tipo = get_field('tipo_de_planta');?>
				<ul  class="nav nav-pills tabs-proyectos tabs-plantas">

					<?php if ($tipo == "Departamento"):?>
					<?php else:?>
					<li class="active">
						<a href="#portada" data-toggle="tab">Fachada</a>
					</li>
					<?php endif;?>

					<?php
					// Si tiene varias fotografías
					while ( have_rows('fotografias_plantas_loop') ) : the_row();
					$titulo_tab 	= get_sub_field('titulo_pestana');
					$personalizar 	= get_sub_field('personalizar_pestana');
					$count++;
					?>

						<?php if ($count == 1):?>
						<li>
						<?php else:?>
						<li>
						<?php endif;?>
							<a href="#<?php echo $count;?>b" data-toggle="tab">
								<?php  if ($personalizar):?>
								<?php echo $personalizar;?>
								<?php else:?>
								<?php echo $titulo_tab;?>
								<?php endif;?>
							</a>
						</li>
						<?php endwhile;?>
					</ul>
					<div class="tab-content clearfix">
							<?php if ($tipo == "Departamento"):?>
													<script>
						$( document ).ready(function() {
						$('ul.tabs-plantas li').first().addClass('active');
						$('#1b').addClass('active');
						});
					</script>

							<?php else:?>
						<div class="tab-pane active" id="portada">
							<?php
							$fotografia_portada 	= get_field('fotografia_planta');
							$size 					= 'full';
							$image_array_portada 	= wp_get_attachment_image_src($fotografia_portada, $size);
							$image_url_portada 		= $image_array_portada[0];
							?>
							<!-- Fotografía Fachada -->
							<div class="box">
								<?php if ($estado == "Agotado"):?>
								<div class="ribbon ribbon-top-left"><span><?php the_field('estado');?></span></div>
								<?php endif;?>
								<div class="foto_portada_planta_tab">
									<?php if($fotografia_portada):?>

									<a href="<?php echo $image_url_portada;?>" data-fancybox="planta" data-caption="<h4><?php echo $vincular->post_title;?></h4>Planta: <?php the_title();?> ">
										<div class="solo-celular">

											<img src="<?php echo $image_url_portada;?>" class="foto-planta-grande img-responsive tooltipster" title="Ampliar" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>"/>

										</div>
										<div class="solo-desktop">

											<img src="<?php echo $image_url_portada;?>" class="foto-planta-grande img-responsive tooltipster" title="Ampliar" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>" />
										</div>
									</a>

									<?php else:?>
									<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto3.jpg" class="img-responsive tooltipster" title="Ampliar" alt="Sin Fotografía" />
									<?php endif;?>
								</div>
								<div class="ayuda"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</div>
							</div>
						</div>
					<?php endif;?>


						<?php while ( have_rows('fotografias_plantas_loop') ) : the_row();
						$fotografias = get_sub_field('fotografias_planta');
						$size = 'full';
						$image_array = wp_get_attachment_image_src($fotografias, $size);
						$image_url = $image_array[0];
						$conteo++;
						?>
						<div class="tab-pane" id="<?php echo $conteo;?>b">
							<div class="box">
								<div class="foto_portada_planta_tab">
									<?php if ($estado == "Agotado"):?>
									<div class="ribbon ribbon-top-left"><span><?php the_field('estado');?></span></div>
									<?php endif;?>

								<a href="<?php echo $image_url;?>" data-fancybox="planta" data-caption="<h4><?php echo $vincular->post_title;?></h4>Planta: <?php the_title();?> ">
									<div class="solo-celular">
										<img src="<?php echo $image_url;?>" class="foto-planta-grande img-responsive tooltipster" title="Ampliar" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>" />
									</div>
									<div class="solo-desktop">
										<img src="<?php echo $image_url;?>" class="foto-planta-grande img-responsive tooltipster" alt="Planta <?php the_title();?> - <?php echo $vincular->post_title;?>" title="Ampliar" />
									</div>
								</a>

								</div>
								<div class="ayuda"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</div>
							</div>
						</div>
						<?php endwhile;?>
					</div>
					<?php endif;?>
					<?php endif;?>
					<?php
					// variables
					$tipo = get_field('tipo_de_planta');
					$superterreno = get_field('superficie_terreno');
					$superterraza = get_field('superficie_terraza');
					$superedificada = get_field('superficie_edificada');
					$supertotal = get_field('superficie_total');
					$descripcion = get_field('descripcion_breve_pagina_del_proyecto');
					$orientacion = get_field('orientacion');
					$superjardin = get_field('jardin');
					$superjardin_cop = get_field('jardin_copy');
					?>
				</div>
				<div class="col-md-4 no-padding-left">
					<a href="<?php bloginfo('url');?>/proyectos/<?php echo $vincular->post_name;?>/#plantas"><div class="volver tooltipster2" style="width:auto; float:right;" data-tooltipster='{"side":"left"}'"  title="Volver a <?php echo $vincular->post_title;?>"><i class="fas fa-chevron-left"></i> Volver</div></a>
					<div class="bloque-informacion-plantas-single">
						<h1 class="pull-left"><?php the_title();?></h1>
						<div class="informacion-planta" style="padding-top:20px;">

						<!-- Esquicio -->
						<?php
						$esquicio = get_field('esquicio');
						$esquicio_array = wp_get_attachment_image_src($esquicio, 'esquicios');
						$esquicio_array_grande = wp_get_attachment_image_src($esquicio, 'full');
						?>
						<?php if ($esquicio):?>
						<div class="col-md-12">
							<div class="row">

								<a href="<?php echo $esquicio_array_grande[0];?>" data-fancybox="planta" data-caption="<h4><?php echo $vincular->post_title;?></h4>Esquicio Planta: <?php the_title();?>">


								<img src="<?php echo $esquicio_array[0];?>" class="img-responsive tooltipster" title="Ampliar" alt="Esquicio Planta <?php the_title();?>"/>

							</a>
								<small><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</small>
							</div>
						</div>
						<?php endif;?>
						<!-- Fin Esquicio -->


							<!-- Descripción Larga -->
							<?php
							$descripcion_larga = get_field('descripcion_grande_pagina_interior_de_planta');
							if ($descripcion_larga):?>
							<div class="col-md-12">
								<div class="row">
									<div class="info_planta_contenido">
										<p class="info_planta_contenido_titulo">
											Descripción
										</p>
										<?php the_field('descripcion_grande_pagina_interior_de_planta');?>
									</div>
								</div>
							</div>
							<hr></hr>
							<?php endif;?>
							<?php if ($descripcion_larga):?>
							<?php else:?>
							<!-- Descripción breve más orientación -->
							<div class="row">
								<?php if ($orientacion):?>
								<div class="col-md-6">
									<?php else:?>
									<div class="col-md-12">
										<?php endif;?>
										<div class="info_planta_contenido">
											<p class="info_planta_contenido_titulo">
												Descripción
											</p>
											<?php echo $descripcion;?>
										</div>
									</div>
									<?php if ($orientacion):?>
									<div class="col-md-6 text-right">
										<div class="info_planta_contenido">
											<p class="info_planta_contenido_titulo">
												<i class="fas fa-street-view"></i> Orientación
											</p>
											<?php echo $orientacion;?>
										</div>
									</div>
									<?php endif;?>
								</div>
								<hr></hr>
								<?php endif;?>
								<!-- Superficies -->
								<div class="row">
									<div class="col-md-7">
										<div class="info_planta_contenido">
											<p class="info_planta_contenido_titulo">
												Superficies
											</p>
											<?php if ($tipo == "Departamento"):?>
												<?php if ($superterraza):?>
												<li><strong>Terraza:</strong> <?php echo $superterraza;?> m²</li>
												<?php endif;?>
											<?php else:?>
												<?php if ($superterreno):?>
												<li><strong>Terreno:</strong> <?php echo $superterreno;?> m²</li>
												<?php endif;?>
											<?php endif;?>

											<?php if ($superjardin):?>
											<li><strong>Jardín:</strong> <?php echo $superjardin;?> m²</li>
											<?php endif;?>

											<?php if ($superjardin_cop):?>
											<li><strong>Jardinera:</strong> <?php echo $superjardin_cop;?> m²</li>
											<?php endif;?>

											<?php if ($superedificada):?>
											<li><strong>Construida:</strong> <?php echo $superedificada;?> m²</li>
											<?php endif;?>

											<?php if ($supertotal):?>
											<li><strong>Total: <?php echo $supertotal;?> m²</strong></li>
											<?php endif;?>


										</div>
									</div>
									<?php 
									$pregunta_adicional = get_field('¿quieres_agregar_un_campo_personalizado');
									$titulo_campo_personalizado = get_field('titulo_campo_personalizado');
									$contenido_campo_personalizado = get_field('contenido_campo_personalizado');
									if ($pregunta_adicional == "Si"):?>
									<div class="col-md-12">
										<hr></hr>
										<div class="info_planta_contenido">
											<p class="info_planta_contenido_titulo">
												<?php echo $titulo_campo_personalizado;?>
											</p>
											<li><?php echo $contenido_campo_personalizado;?></li>
										</div>
									</div>
									<?php endif;?>
									<!-- Superficie Total -->
									<?php if ($supertotal):?>
									<!-- <div class="col-md-5 text-right texto-rojo">
										<span><strong>Total</strong></span>
										<h1><strong><?php echo $supertotal;?></strong></h1>
										<span class="total-metros">m²</span>
									</div> -->
									<?php endif;?>
								</div>
								<hr></hr>
								<!-- Pestañas de Formulario y agenda -->
								<div class="col-md-12">
									<div class="row">
										<ul  class="nav nav-pills tabs-solicitar-agenda">
											<li class="active"><a href="#1C" data-toggle="tab">Solicitar Información</a></li>
											<li><a href="#2C" data-toggle="tab">Sala de</br> Ventas</a></li>
										</ul>
										<div class="tab-content clearfix">
											<div class="tab-pane active" id="1C">
												<!-- Formulario -->
												<script>
												// Variables para Formulario
												$(document).ready(function() {
												$('input[name="proyecto"]').val('<?php echo $vincular->post_title;?>');
												});
												dataLayer.push({
												'Planta': '<?php the_title();?>',
												});
												</script>
												<div class="espacio-form"></div>
												<div id="gracias-form-solicitar-planta">
													<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado.svg" class="img-responsive icon-enviado" alt="Mensaje Enviado">
													<h3>Gracias <span class="nombre-persona"></span></h3>
													<p><strong>Tu solicitud ha sido enviada con éxito.</strong></p>
													<p>Pronto te contactaremos.</p>
												</div>
												<div id="formulario-solicitar-planta">
													<?php echo do_shortcode('[contact-form-7 id="146" title="Solicitar Información por planta" html_id="formulario-planta"]');?>
												</div>
											</div>
											<div class="tab-pane agenda-tu-visita-tab" id="2C">
												<?php
												$vincular = get_field('vincular_planta_a_proyecto');
												$idproyecto = $vincular->ID;
												?>
												<div class="separador-simple"></div>
												<p class="info_planta_contenido_titulo">
													Visita nuestra Sala de Ventas
												</p>
												<?php
													$telefono1 = get_field('telefono', 	$idproyecto);
													$telefono2 = get_field('telefono_2', 	$idproyecto);
													$direccion = get_field('direccion', 	$idproyecto);
													$email = get_field('correo_electronico', 	$idproyecto);
													$horario_visita = get_field('horario_visita', 	$idproyecto);
													$horario_sala_de_ventas = get_field('horario_sala_de_ventas', 	$idproyecto);
													$coordenadas = get_field('ingresa_coordenadas', 	$idproyecto);
												?>
												<?php if ($direccion):?><i class="fas fa-map-marker-alt"></i> <?php echo $direccion;?><?php endif;?>
												<hr></hr>
												<?php if ($telefono1):?><a href="tel:<?php echo $telefono1;?>" class="tooltipster2" title="Llamar"><i class="fas fa-phone"></i> <?php echo $telefono1;?></a><?php endif;?> <?php if ($telefono2):?> - <a href="tel:<?php echo $telefono2;?>" class="tooltipster2" title="Llamar"><i class="fas fa-phone"></i> <?php echo $telefono2;?></a><?php endif;?>
												<hr></hr>
												<?php if ($email):?><a href="mailto:<?php echo $email;?>" class="tooltipster2" title="Enviar Email"><i class="fas fa-envelope"></i> <?php echo $email;?></a><?php endif;?>
												<hr></hr>
												<?php if ($horario_visita):?>
												<p><i class="far fa-clock"></i> <?php echo $horario_visita;?></p>
												<?php endif;?>
												<hr></hr>
												<?php if ($coordenadas):?>
												<div class="solo-celular">
													<a href="waze://?ll=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>&amp;navigate=yes" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/waze.svg" alt="Dirección en Waze: <?php echo $direccion;?>" class="img-responsive icon-svg" /></a>
													<a href="http://maps.google.com/maps?daddr=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/googlemaps.svg" alt="Dirección en Google Maps <?php echo $direccion;?>" class="img-responsive icon-svg" /></a>
												</div>
												<?php endif;?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php get_template_part('modulos/popup');?>
		<?php get_footer();?>
