<?php
/**
 * Template Name: Proceso de compra
*/
get_header(); 
get_template_part('secciones/header-normal');
?>

<style>
/*===SECTION TABS===*/

.section-tabs{margin-bottom:100px;}

.tab-pane-pcompra{padding-top:60px;}

.col-sm-left{padding-left:0px;}

.col-sm-right{padding-right:0px;}

/*Titulos de las columnas*/
.col-title{
    border-bottom:3px solid #7C7D7E; 
    width:100%; 
    padding:0; 
    margin-bottom:10px;
}

.col-title h1{
    font-weight:bold; 
    padding-bottom:10px;
}

.col-title span{
    border: 3px solid #7C7D7E; 
    padding:5px 15px; 
    border-radius:50%; 
    font-weight:bold; 
    font-size:20px;
}
/*Fin Titulos de las columnas*/

/*Columnas de pre evaluacion*/

.pre-eva-col h5{
    text-align:center; 
    color:#F6001A; 
    border-top:1px solid #E4E4E4; 
    border-bottom:1px solid #E4E4E4; 
    padding:5px 0 5px 0;
}

.pre-eva-col ul{margin:0; padding:0;}

.pre-eva-col ul li{
    list-style-type: square;
    margin-bottom:5px;
}

</style>

<div class="container-fluid" style="padding-right:0px; padding-left:0px;">
    <picture>
        <source media="(max-width: 920px)" srcset="<?php bloginfo('template_url');?>/recursos/img/slide-proceso-compra-mobile.jpg">
        <source media="(min-width: 920px)" srcset="<?php bloginfo('template_url');?>/recursos/img/slide-proceso-compra.jpg">
        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/slide-proceso-compra.jpg" alt="Proceso de compra Avellaneda">
    </picture>
</div>

<!-- ================================================================================================================== -->
<!-- ======================================== Sección Pagina Interior ======================================== -->
<!-- ================================================================================================================== -->
<section class="pagina-interior">

    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="<?php bloginfo('url');?>">Inicio</a>
            </li>
            <?php if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)):?>

            <?php else:?>
            <li>
                <?php echo get_the_title($parent); ?>
            </li>
            <?php endif;?>

            <li class="active">
                <?php the_title();?>
            </li>
        </ul>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php the_content();?>

    <?php endwhile; else : ?>
        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

    </div>
</section>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Tabs ======================================== -->
<!-- ================================================================================================================== -->
<section class="section-tabs">
	<div id="equipamiento" class="container">
		<div class="solo-desktop">
			<ul class="nav nav-pills tabs-proyectos">
                <li class="active">
				    <a id="tabProyecto" href="#1b" data-toggle="tab">
						<div class="solo-desktop">COTIZACIÓN</div>
					</a>
				</li>
                <li>
				    <a id="tabProyecto" href="#2b" data-toggle="tab">
						<div class="solo-desktop">RESERVA</div>
					</a>
				</li>
                <li>
				    <a id="tabProyecto" href="#3b" data-toggle="tab">
						<div class="solo-desktop">PRE-EVALUACIÓN</div>
					</a>
				</li>
                <li>
				    <a id="tabProyecto" href="#4b" data-toggle="tab">
						<div class="solo-desktop">PROMESA</div>
					</a>
				</li>
                <li>
				    <a id="tabProyecto" href="#5b" data-toggle="tab">
						<div class="solo-desktop">ESCRITURA</div>
					</a>
				</li>
                <li>
				    <a id="tabProyecto" href="#6b" data-toggle="tab">
						<div class="solo-desktop">ENTREGA</div>
					</a>
				</li>					
			</ul>
		</div>
		
        <div class="solo-celular">
			<ul  class="nav nav-pills tabs-proyectos tabs-celular">
				<span class="btn-siguiente-tab"><i class="fas fa-chevron-right"></i></span>
				<li class="active">
					<a id="tabProyecto" href="#1c" data-toggle="tab">
						<div class="tab-grande">COTIZACIÓN</div>
					</a>
				</li>
				<li>
					<a id="tabProyecto" href="#2c" data-toggle="tab">
						<div class="tab-grande">RESERVA</div>
					</a>
				</li>
                <li>
					<a id="tabProyecto" href="#3c" data-toggle="tab">
						<div class="tab-grande">PRE-EVALUACIÓN</div>
					</a>
				</li>
                <li>
					<a id="tabProyecto" href="#4c" data-toggle="tab">
						<div class="tab-grande">PROMESA</div>
					</a>
				</li>
                <li>
					<a id="tabProyecto" href="#5c" data-toggle="tab">
						<div class="tab-grande">ESC. DE COMPRAVENTA</div>
					</a>
				</li>
                <li>
					<a id="tabProyecto" href="#6c" data-toggle="tab">
						<div class="tab-grande">ENTREGA</div>
					</a>
				</li>
				<span class="btn-anterior-tab"><i class="fas fa-chevron-left"></i></span>
			</ul>
		</div>
		
        <div class="solo-desktop">
		    <div class="tab-content clearfix">
				<!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra active" id="1b">
                    <div class="col-sm-5 col-sm-left">
                        <img src="<?php bloginfo('template_url');?>/recursos/img/cotizacion.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-7 col-sm-right">
                        <div class="container col-title">
                            <div class="col-sm-6 col-sm-left" style="text-align:left;"><h2>COTIZACIÓN</h2></div>
                            <div class="col-sm-6 col-sm-right" style="text-align:right;"><span>1</span></div>
                        </div>             
                        Ingresa a Avellaneda.cl y visita nuestros proyectos de casas y departamentos, donde podrás encontrar de  acuerdo a tus necesidades la vivienda que estás buscando.  Acércate  a la sala de venta del proyecto de tu interés, donde podrás conocer las diferentes tipologías de casas o departamentos que tenemos y recorrer el proyecto  en su totalidad, visitar pilotos y conocer la disponibilidad o características particulares que te interesen.
                        <p style="margin-top:20px;">Un equipo de vendedores capacitados te atenderá y realizará una  cotización según lo que te acomode. También tienes la opción de cotizar a través de nuestra página web o comunicándote telefónicamente con nuestra sala de venta.</p>                   
                    </div>
				</div>
				<!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra" id="2b">
                    <div class="col-sm-7 col-sm-left">
                        <div class="container col-title">
                            <div class="col-sm-6 col-sm-left" style="text-align:left;"><h2>RESERVA</h2></div>
                            <div class="col-sm-6 col-sm-right" style="text-align:right;"><span>2</span></div>
                        </div>
                        Una vez que te hayas decidido por una  vivienda Avellaneda,  acercarte a la  sala de venta para hacer la reserva, el monto representa la seriedad del compromiso que asumimos las dos partes y permite garantizar que la unidad quede bloqueada para su venta a otro interesado, esta tiene una validez de 10 días.          
                    </div>

                    <div class="col-sm-5 col-sm-right">
                        <img src="<?php bloginfo('template_url');?>/recursos/img/reserva.jpg" alt="">
                    </div>
				</div>
                <!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra" id="3b">
                    <div class="col-sm-7 col-sm-left">
                        <div class="container col-title">
                            <div class="col-sm-6 col-sm-left" style="text-align:left;"><h2>PRE-EVALUACIÓN</h2></div>
                            <div class="col-sm-6 col-sm-right" style="text-align:right;"><span>3</span></div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-sm-right"></div>

                    <p style="clear:both;">En este proceso, contamos con un equipo de ejecutivas comerciales con vasta experiencia y con contactos directos con el área hipotecaria de las instituciones financieras, que te podrán asesorar con las posibilidades de financiamiento necesario para la compra de tu casa o departamento Avellaneda.</p>

                    <p style="margin-top:20px;">En caso de que requieras de nuestra ayuda, es necesario que nos envíes la documentación que se detalla a continuación, según tú forma de compra.</p>

                    <h1 style="margin:30px; text-align:center;">Documentos necesario para la pre–evaluación:</h2>

                    <div class="container">
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Antecedentes personales del comprador:</h5>
                            <ul>
                                <li>Fotocopia C.I por ambos lados del solicitante y su cónyuge en caso necesario.</li>
                                <li>Si es casado, certificado de matrimonio.</li>
                                <li>Libreta de ahorro y fotocopia de certificado de subsidio en caso de proceder.</li>
                                <li>En caso de contar con cuenta corriente, señalar banco y N° de cuenta.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Estado situación patrimonial:</h5>
                            <ul>
                                <li>Vehículo (permiso de circulación)</li>
                                <li>Bien Raíz (fotocopia escritura pública y si se está pagando dividendos adjuntar último pago)</li>
                                <li>Depósitos (fondos mutuos, depósitos a plazo….)</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Persona natural dependiente:</h5>
                            <ul>
                                <li>Certificado de renta y antigüedad laboral.</li>
                                <li>6 últimas liquidaciones de renta.</li>
                                <li>24 últimas cotizaciones previsionales.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="container" style="margin-top:30px;">
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Persona natural Independiente:</h5>
                            <ul>
                                <li>Título profesional.</li>
                                <li>2 últimas declaraciones impuesto a la renta.</li>
                                <li>6 últimas boletas o 6 últimos P.P.M.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Empresas:</h5>
                            <ul>
                                <li>2 últimas declaraciones de impuestos a la renta.</li>
                                <li>3 últimos balances.</li>
                                <li>Escritura de constitución de la sociedad, con certificado de vigencia.</li>
                                <li>Poderes de los representantes legales.</li>
                                <li>12 últimos IVA.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <a href="http://www.avellaneda.cl/antecedentes.pdf" target="_blank"><img src="<?php bloginfo('template_url');?>/recursos/img/pdf-listado-docs.png" alt=""></a>
                        </div>
                    </div>
				</div>
                <!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra" id="4b">
                    <div class="col-sm-5 col-sm-left">
                        <img src="<?php bloginfo('template_url');?>/recursos/img/promesa.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-7 col-sm-right">
                        <div class="container col-title">
                            <div class="col-sm-6 col-sm-left" style="text-align:left;"><h2>PROMESA</h2></div>
                            <div class="col-sm-6 col-sm-right" style="text-align:right;"><span>4</span></div>
                        </div>             
                        Para formalizar el proceso de compra, la ejecutiva comercial te contactará para que te acerques a nuestra oficina central para firmar la promesa de compraventa. En este instrumento legal debe quedar documentado o cancelado el <strong>pago del pie.</strong>

                        <p style="margin-top:20px;">Recuerda que es necesaria  la pre aprobación o aprobación financiera en caso de que la compra de tu vivienda incluya un crédito hipotecario, al momento de firmar la promesa.</p>

                        <p style="margin-top:20px;">En la promesa de compraventa encontrarás el detalle de la propiedad que estás comprando, así como adicionales (estacionamientos y/o bodegas), el precio, el <strong>plan de pago del pie</strong> y la fecha estimada de recepción final del proyecto. Con la Recepción Final Municipal se puede proceder a la inscripción del proyecto en el Conservador de Bienes Raíces respectivo y el posterior estudio de títulos de los bancos, para que elaboren la Escritura de Compraventa</p>              
                    </div>
				</div>
                <!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra" id="5b">
                    <div class="col-sm-7 col-sm-left">
                        <div class="container col-title">
                            <div class="col-sm-9 col-sm-left" style="text-align:left;"><h2>ESCRITURA DE COMPRAVENTA</h2></div>
                            <div class="col-sm-3 col-sm-right" style="text-align:right;"><span>5</span></div>
                        </div>
                        Una vez que haya concluido la etapa de construcción del proyecto y se tengan todos los certificados  necesarios, se procede a la recepción municipal de éste, minuto en el cual comienza la etapa de pre entrega de la vivienda y de escrituración de ésta.

                        <p style="margin-top:20px;">La escritura de compraventa, es el documento legal que reemplaza a la promesa y en donde se fijan todas las condiciones de la compra de la propiedad. En esta escritura, participan todos los actores involucrados en este proceso, es decir, tú como comprador, la inmobiliaria y en caso de existir un crédito hipotecario, la institución financiera que lo otorga, la que es encargada de realizar la escritura de compraventa y de citar a las partes a su firma.</p>

                        <p style="margin-top:20px;">La escritura debe firmarse en notaría y luego ésta se inscribe en el Conservador de Bienes Raíces respectivo, en donde una vez concluidos estos trámites la propiedad queda a tú nombre.</p>    
                    </div>

                    <div class="col-sm-5 col-sm-right">
                        <img src="<?php bloginfo('template_url');?>/recursos/img/escritura.jpg" alt="">
                    </div>
                </div>				
                <!-- Tab  -->
				<div class="tab-pane tab-pane-pcompra" id="6b">
                    <div class="col-sm-5 col-sm-left">
                        <img src="<?php bloginfo('template_url');?>/recursos/img/entrega.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-7 col-sm-right">
                        <div class="container col-title">
                            <div class="col-sm-6 col-sm-left" style="text-align:left;"><h2>ENTREGA</h2></div>
                            <div class="col-sm-6 col-sm-right" style="text-align:right;"><span>6</span></div>
                        </div>             
                        Una vez firmada la escritura y cancelado todos los saldos pendientes, nos comunicaremos contigo para coordinar un día y hora para hacer entrega de tu vivienda Avellaneda.

                        <p style="margin-top:20px;">En la entrega se debe firmar el “acta de entrega” en donde se registran los consumos de los servicios generales como  luz,  agua y  gas, y si  procede, se indican  las  posibles observaciones que pudieran existir, las cuales deben quedar detalladas en dicha acta.</p>

                        <p style="margin-top:20px;">Una vez firmada el acta, se te hará entrega de las llaves de tu vivienda Avellaneda junto con el manual de usuario, los planos de arquitectura e instalaciones y  las especificaciones técnicas, entre otros documentos (Reglamento de Copropiedad si corresponde).</p>
                    </div>
				</div>
            </div>
        </div>
		
        <!-- Tabs solo para celular -->
		<div class="solo-celular">
			<div class="tab-content clearfix">
				<div class="tab-pane active" id="1c">
                    <div class="col-sm-6 col-sm-left col-sm-right">
                        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/cotizacion.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-6 col-sm-left col-sm-right" style="margin-top:30px;">
                        <div class="container col-title">
                            <div class="col-xs-6 col-sm-left" style="text-align:left;"><h2>COTIZACIÓN</h2></div>
                            <div class="col-xs-6 col-sm-right" style="text-align:right;"><span>1</span></div>
                        </div>             
                        Ingresa a Avellaneda.cl y visita nuestros proyectos de casas y departamentos, donde podrás encontrar de  acuerdo a tus necesidades la vivienda que estás buscando.  Acércate  a la sala de venta del proyecto de tu interés, donde podrás conocer las diferentes tipologías de casas o departamentos que tenemos y recorrer el proyecto  en su totalidad, visitar pilotos y conocer la disponibilidad o características particulares que te interesen.
                        <p style="margin-top:20px;">Un equipo de vendedores capacitados te atenderá y realizará una  cotización según lo que te acomode. También tienes la opción de cotizar a través de nuestra página web o comunicándote telefónicamente con nuestra sala de venta.</p>                   
                    </div>
                </div>

                <div class="tab-pane" id="2c">
                    <div class="col-sm-5 col-sm-left col-sm-right">
                        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/reserva.jpg" alt="">
                    </div>
                    <div class="col-sm-7 col-sm-left col-sm-right" style="margin-top:30px;">
                        <div class="container col-title">
                            <div class="col-xs-6 col-sm-left" style="text-align:left;"><h2>RESERVA</h2></div>
                            <div class="col-xs-6 col-sm-right" style="text-align:right;"><span>2</span></div>
                        </div>
                        Una vez que te hayas decidido por una  vivienda Avellaneda,  acercarte a la  sala de venta para hacer la reserva, el monto representa la seriedad del compromiso que asumimos las dos partes y permite garantizar que la unidad quede bloqueada para su venta a otro interesado, esta tiene una validez de 10 días.          
                    </div>
                </div>

                <div class="tab-pane" id="3c">
                    <div class="col-sm-7 col-sm-left">
                        <div class="container col-title" style="margin-top:30px;">
                            <div class="col-xs-8 col-sm-left" style="text-align:left;"><h2>PRE-EVALUACIÓN</h2></div>
                            <div class="col-xs-4 col-sm-right" style="text-align:right;"><span>3</span></div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-sm-right"></div>

                    <p style="clear:both;">En este proceso, contamos con un equipo de ejecutivas comerciales con vasta experiencia y con contactos directos con el área hipotecaria de las instituciones financieras, que te podrán asesorar con las posibilidades de financiamiento necesario para la compra de tu casa o departamento Avellaneda.</p>

                    <p style="margin-top:20px;">En caso de que requieras de nuestra ayuda, es necesario que nos envíes la documentación que se detalla a continuación, según tú forma de compra.</p>

                    <h1 style="margin:30px; text-align:center;">Documentos necesario para la pre–evaluación:</h2>

                    <div class="container">
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Antecedentes personales del comprador:</h5>
                            <ul>
                                <li>Fotocopia C.I por ambos lados del solicitante y su cónyuge en caso necesario.</li>
                                <li>Si es casado, certificado de matrimonio.</li>
                                <li>Libreta de ahorro y fotocopia de certificado de subsidio en caso de proceder.</li>
                                <li>En caso de contar con cuenta corriente, señalar banco y N° de cuenta.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Estado situación patrimonial:</h5>
                            <ul>
                                <li>Vehículo (permiso de circulación)</li>
                                <li>Bien Raíz (fotocopia escritura pública y si se está pagando dividendos adjuntar último pago)</li>
                                <li>Depósitos (fondos mutuos, depósitos a plazo….)</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Persona natural dependiente:</h5>
                            <ul>
                                <li>Certificado de renta y antigüedad laboral.</li>
                                <li>6 últimas liquidaciones de renta.</li>
                                <li>24 últimas cotizaciones previsionales.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="container" style="margin-top:30px;">
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Persona natural Independiente:</h5>
                            <ul>
                                <li>Título profesional.</li>
                                <li>2 últimas declaraciones impuesto a la renta.</li>
                                <li>6 últimas boletas o 6 últimos P.P.M.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <h5>Empresas:</h5>
                            <ul>
                                <li>2 últimas declaraciones de impuestos a la renta.</li>
                                <li>3 últimos balances.</li>
                                <li>Escritura de constitución de la sociedad, con certificado de vigencia.</li>
                                <li>Poderes de los representantes legales.</li>
                                <li>12 últimos IVA.</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 pre-eva-col">
                            <a href="#" target="_blank"><img src="<?php bloginfo('template_url');?>/recursos/img/pdf-listado-docs.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="4c">
                    <div class="col-sm-5 col-sm-left col-sm-right">
                        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/promesa.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-7 col-sm-left col-sm-right">
                        <div class="container col-title" style="margin-top:30px;">
                            <div class="col-xs-6 col-sm-left" style="text-align:left;"><h2>PROMESA</h2></div>
                            <div class="col-xs-6 col-sm-right" style="text-align:right;"><span>4</span></div>
                        </div>             
                        Para formalizar el proceso de compra, la ejecutiva comercial te contactará para que te acerques a nuestra oficina central para firmar la promesa de compraventa. En este instrumento legal debe quedar documentado o cancelado el pago del pie.

                        <p style="margin-top:20px;">Recuerda que es necesaria  la pre aprobación o aprobación financiera en caso de que la compra de tu vivienda incluya un crédito hipotecario, al momento de firmar la promesa.</p>

                        <p style="margin-top:20px;">En la promesa de compraventa encontrarás el detalle de la propiedad que estás comprando, así como adicionales (estacionamientos y/o bodegas), el precio, el plan de pago del pie y la fecha estimada de recepción final del proyecto. Con la Recepción Final Municipal se puede proceder a la inscripción del proyecto en el Conservador de Bienes Raíces respectivo y el posterior estudio de títulos de los bancos, para que elaboren la Escritura de Compraventa</p>              
                    </div>
                </div>

                <div class="tab-pane" id="5c">
                    <div class="col-sm-5 col-sm-left col-sm-right">
                        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/escritura.jpg" alt="">
                    </div>

                    <div class="col-sm-7 col-sm-left">
                        <div class="container col-title col-sm-right" style="margin-top:30px;">
                            <div class="col-xs-9 col-sm-left" style="text-align:left;"><h2>ESCRITURA DE COMPRAVENTA</h2></div>
                            <div class="col-xs-3 col-sm-right" style="text-align:right;"><span>5</span></div>
                        </div>
                        Una vez que haya concluido la etapa de construcción del proyecto y se tengan todos los certificados  necesarios, se procede a la recepción municipal de éste, minuto en el cual comienza la etapa de pre entrega de la vivienda y de escrituración de ésta.

                        <p style="margin-top:20px;">La escritura de compraventa, es el documento legal que reemplaza a la promesa y en donde se fijan todas las condiciones de la compra de la propiedad. En esta escritura, participan todos los actores involucrados en este proceso, es decir, tú como comprador, la inmobiliaria y en caso de existir un crédito hipotecario, la institución financiera que lo otorga, la que es encargada de realizar la escritura de compraventa y de citar a las partes a su firma.</p>

                        <p style="margin-top:20px;">La escritura debe firmarse en notaría y luego ésta se inscribe en el Conservador de Bienes Raíces respectivo, en donde una vez concluidos estos trámites la propiedad queda a tú nombre.</p>    
                    </div>
                    
                </div>

                <div class="tab-pane" id="6c">
                    <div class="col-sm-5 col-sm-left col-sm-right">
                        <img width="100%" src="<?php bloginfo('template_url');?>/recursos/img/entrega.jpg" alt="">
                    </div>
                    
                    <div class="col-sm-7 col-sm-left col-sm-right">
                        <div class="container col-title" style="margin-top:30px;">
                            <div class="col-xs-6 col-sm-left" style="text-align:left;"><h2>ENTREGA</h2></div>
                            <div class="col-xs-6 col-sm-right" style="text-align:right;"><span>6</span></div>
                        </div>             
                        Una vez firmada la escritura y cancelado todos los saldos pendientes, nos comunicaremos contigo para coordinar un día y hora para hacer entrega de tu vivienda Avellaneda.

                        <p style="margin-top:20px;">En la entrega se debe firmar el “acta de entrega” en donde se registran los consumos de los servicios generales como  luz,  agua y  gas, y si  procede, se indican  las  posibles observaciones que pudieran existir, las cuales deben quedar detalladas en dicha acta.</p>

                        <p style="margin-top:20px;">Una vez firmada el acta, se te hará entrega de las llaves de tu vivienda Avellaneda junto con el manual de usuario, los planos de arquitectura e instalaciones y  las especificaciones técnicas, entre otros documentos (Reglamento de Copropiedad si corresponde).</p>
                    </div>
                </div>
			</div>
		</div>
	</section>


<?php get_footer();?>