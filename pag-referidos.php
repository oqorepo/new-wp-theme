<?php
/**
* Template Name: Página Referidos
*/
get_header();
get_template_part('secciones/header-normal');
?>



<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot').addClass('activo');

// Google Tag Manager
dataLayer.push({
'tipoPagina': 'Referidos',
});



});
</script>

<?php $option = $_GET['opcion'];?>

	<script>
$(document).ready(function() {
$('.muestraselectorproyectos').hide();
});
</script>

<?php if(isset($_GET['opcion'])) :?>


<?php else:?>
	<script>
$(document).ready(function() {
$('select[name="ProyectosReferidos"]').removeClass('hide');
$('.muestraselectorproyectos').show();
});
</script>


<?php endif;?>

<!-- ============================================================================= -->
<!-- Sección Título -->
<!-- ============================================================================= -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco"><?php the_title();?></h1>
	</div>
</section>


<div class="container">


<?php
							$imagen 		= get_field('imagen');
														$size 						= 'full';
		$imagen_array = wp_get_attachment_image_src($imagen, $size);
?>


<img src="<?php echo $imagen_array[0];?>" class="img-responsive img-full" alt="<?php the_title();?>" /
>
</div>


<section class="pagina-interior">
	<div class="container">
		<div class="col-md-4">
				
	
		
		<?php the_field('bloque_izquierdo');?>

		

		</div>


		<div id="gracias-formulario-referidos" style="display:none;">
			<div class="col-md-8 text-center">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado2.svg" class="img-responsive icon-enviado" alt="Enviado">
				<h2>Gracias <span class="nombre-persona"></span></h2>
				<p><strong>Tus datos han sido recibidos.</strong></p>
			</div>
		</div>


		<div id="formulario-referidos">
			<div class="col-md-8">
				<?php echo do_shortcode('
				[contact-form-7 id="1237" title="Referidos" html_id="formulario-referidos"]');?>
			</div>
		</div>


	</div>
</section>

<script>
$(document).ready(function() {
var referidos = '<?php echo $option;?>';

$('select[name="ProyectosReferidos"]').val(referidos);
});

</script>
<?php get_footer();?>