<?php
/**
* Template Name: Página Oportunidades v2
*/
get_header();
?>

<script>
	$(document).ready(function () {
		$('.head').css('border', '0px');
		// Google Tag Manager
		dataLayer.push({
			'tipoPagina': '<?php the_title();?>',
		});
	});
</script>

<?php get_template_part('secciones/header-normal');?>

<?php
$terms = get_the_terms( $post->ID , 'ubicaciones' );
if ( $terms != null ):?>
    <?php foreach( $terms as $term ) :?>
        <?php if ($term->parent == "23"):?>
            <script>
                $('.head').css('border-bottom', '0px');
            </script>
        <?php elseif ($term->parent == "25"):?>
            <script>
                $('.head').css('border-bottom', '0px');
            </script>
        <?php endif;?>
    <?php endforeach;?>
<?php endif;?>

<!-- ============================================================================= -->
<!-- Sección Título 
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">Oportunidades</h1>
	</div>
</section>
-->
<!-- ============================================================================= -->

<!-- ============================================================================= -->
<!-- Sección Proyectos -->
<!-- ============================================================================= -->


<div class="separador-de-seccion" style="padding:60px 0 80px;">
    <div class="container">
        <div class="row av-promo--block" style="display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; flex-wrap: wrap;">
            <?php 
            $ids = get_field('seleccionar_proyectos', false, false);
            
			$post_id          = get_the_ID();
			$hoy              = date( 'Ymd', current_time( 'timestamp', 0 ));

			$_posts = new WP_Query( array(
				'post_type'         => 'proyectos',
				'posts_per_page'    => -1,
				'post__in'			=> $ids,
				'post_status'		=> 'publish',
				'orderby'        	=> 'post__in',
			));

			if ( $_posts->have_posts() ) : ?>
			<div class="col-md-4" style="display: flex;flex-direction: column; margin:10px 0;">
				<div style="height: 100%;display: flex;flex-direction: column;  background:url('<?php echo bloginfo('template_url');?>/recursos/img/oportunidades-bg-img.jpg') no-repeat; background-size:cover;">
				<h2 class="text-center" style="color:white; padding-top:40px;">OPORTUNIDADES</h2>
				<p class="text-center" style="color:white;">Encuentra acá nuestras últimas unidades y ofertas disponibles</p>
			<!-- <img src="<?php bloginfo('template_url');?>/recursos/img/oportunidades-bg-img.jpg" style="background:url('<?php echo $image['url']; ?>');" alt="Oportunidades" style="width:100%"> -->
				</div>
			</div>
				<?php while ( $_posts->have_posts() ) : $_posts->the_post();
			$pronto = get_field('url_landing_proyecto');
			$fotografia_menu = get_field('foto_portada_general');
			$size = 'foto-menu-col4';
			$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
			?>

			<?php if ($pronto):?>
			<a href="<?php echo $pronto;?>" class="tooltipster" title="Ver Proyecto" target="_new" ></a>
			<?php else:?>

				<div class="col-md-4 " style="display: flex;flex-direction: column; margin:10px 0;">
			<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto">
					<?php
					$fotografia = get_field('foto_portada_general');
					$size = 'full';
					$image_array = wp_get_attachment_image_src($fotografia, $size);
					?>
					<div style="border:1px solid #c4c4c4;">
					<?php if ($fotografia):?>
					<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="Proyecto <?php the_title();?>" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full" alt="Sin Fotografía" />
					<?php endif;?>
					<div>
						<div id="chevron" class="av-badge-project">
						</div>
							<p style="font-weight:700; color:white; text-align:center; top:-42px;  position: relative; z-index:9999999;">
								<?php
								// variables
								$tag = get_field('selecciona_tag_del_proyecto');
								$tag_personalizado = get_field('personalizar_tag');
								if ($tag == "Personalizar"):?>
									<?php echo $tag_personalizado;?>
									<?php elseif ($tag == "Normal"):?>
									<?php else:?>
									<?php echo $tag;?>
								<?php endif;?>
							</p>
					</div>
					<div class="listado_de_proyectos altura-proyectos-buscador">
						<h5>
							<?php get_template_part('modulos/ubicacion-actual');?>
						</h5>
						<h4>
							<?php the_title(); ?>
						</h4>
						<hr>
						<p>
							<?php the_field('bloque_rojo');?>
						</p>
					</div>
					</div>
			</a>
				</div>
			<?php endif;?>

			<?php
			endwhile;
			endif;
			wp_reset_postdata();
			?>

		</div>
	</div>
</div>

<div class="separador-de-seccion container-fluid" style="background:#e4e4e4;">
	<div class="container"  style="margin-top:-70px;">
        <div class="row av-promo--block" style="display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; flex-wrap: wrap;">

			<?php
			$query            = new WP_Query(array(
				'post_type'       => array('promociones'),
				'posts_per_page'  => -1,
				'meta_query' => array(
					array(
						'key'		=> 'fecha_de_expiracion',
						'compare'	=> '>=',
						'value'		=> $hoy,
					)
				),
			));
			?>
			<?php if ( $query->have_posts() ) : ?>
			<div class="col-md-12" style=" padding:40px 0;display: flex;flex-direction: column; margin:10px 0 50px;background:url('<?php echo bloginfo('template_url');?>/recursos/img/promociones-img.jpg') no-repeat; background-size:cover;">
				<h2 class="text-center" style="color:white;">PROMOCIONES</h2>
				<p class="text-center" style="color:white;">Encuentra acá nuestras últimas unidades y ofertas disponibles</p>
			<!-- <img src="<?php bloginfo('template_url');?>/recursos/img/oportunidades-bg-img.jpg" style="background:url('<?php echo $image['url']; ?>');" alt="Oportunidades" style="width:100%"> -->
			</div>
				<?php while ( $query->have_posts() ) : $query->the_post(); 
				// Variables
					$imagen = get_field('imagen_promocion_mobile');
					$imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
					$estado = get_field('estado');
					$personalizar = get_field('personalizar');
					$fecha = get_field('fecha_de_expiracion', false, false);
					$fecha = new DateTime($fecha);
					$fechaFormato = $fecha->format('Ymd');
					$hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
					$dateformatstring = "j F Y";
					$unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
					$selecciona = get_field('selecciona_proyecto_o_planta');
					$av_seleccionar_proyecto = get_field('av_np_desde_uf');
				?>
					<!-- Condicionales -->
					<?php  if ($fechaFormato >= $hoy):?>

					<div class="col-md-4" style="display: flex;flex-direction: column; margin:10px 0;">
						<div style="background:white;border:1px solid #c4c4c4;height:100%">
							<a href="<?php the_permalink();?>" id="clickPromocion" onclick="dataLayer.push({'Promo': '<?php the_title();?>'});">
								<div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;">
									<span style="font-size:13px !important; top:42px;">
										<?php if ($estado == "Personalizar"):?>
										<?php echo $personalizar;?>
										<?php else:?>
										<?php echo $estado;?>
										<?php endif;?>
									</span>
								</div>
								<img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster" title="Ver Promoción" alt="<?php the_title();?>"
								style="margin-bottom:15px;" />
							</a>
							<div style="padding:20px;">
								<h4 style="color:black; font-size:15px;">
									<?php the_title();?>
								</h4>
								<hr>
								<p>Vigencia hasta
									<?php echo date_i18n($dateformatstring, $unixtimestamp);?>
									<?php the_field('bloque_rojo', $av_seleccionar_proyecto);?>
								</p>
							</div>
						</div>
					</div>
					<?php endif;?>
			<?php endwhile; endif;?>

		<!-- </div>
		<div class="row"> -->

			<?php

			$post_id          = get_the_ID();
			$hoy              = date( 'Ymd', current_time( 'timestamp', 0 ));
			$query            = new WP_Query(array(
			'post_type'       => array('promociones'),
			'posts_per_page'  => -1,
			'post_status'     => 'publish',
			'order'           => 'DESC',
			'meta_query' => array(
				 array(
					'key'		=> 'fecha_de_expiracion',
					'compare'	=> '<=',
					'value'		=> $hoy,
				)
			),
			));

			if ( $query->have_posts() ) :  while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php // Variables
					$imagen = get_field('imagen_promocion_mobile');
					$imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
					$estado = get_field('estado');
					$personalizar = get_field('personalizar');
					$fecha = get_field('fecha_de_expiracion', false, false);
					$fecha = new DateTime($fecha);
					$fechaFormato = $fecha->format('Ymd');
					$hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
					$dateformatstring = "j F Y";
					$unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
					$selecciona = get_field('selecciona_proyecto_o_planta');
					?>
					<?php  
					// Condicionales
					if ($fechaFormato < $hoy):?>
					<div class="col-md-4" style="display: flex;flex-direction: column; margin:10px 0;">
					<div style="background:white;border:1px solid #c4c4c4;height:100%">
						<img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster blancoynegro" title="Promoción No Vigente"
						alt="<?php the_title();?>" style="margin-bottom:15px;" />
						<div style="padding:20px;">

							<h4 style="color:black; font-size:15px;">
								<?php the_title();?>
							</h4>

							<hr>
							<p>Finalizó el
							<?php echo date_i18n($dateformatstring, $unixtimestamp);?></p>
							

							</div>
						</div>
					</div>
					<?php endif;?>

			<?php endwhile; endif;?>

		</div>
	</div>
</div>

<!-- ============================================================================= -->
<!-- Ganadores -->
<!-- ============================================================================= -->
<?php
	$ganadores            	= new WP_Query(array(
	'post_type'       		=> array('ganadores'),
	'posts_per_page'  		=> -1,
	'post_status'     		=> 'publish',
	'order'           		=> 'DESC',
	));
	if ( $ganadores->have_posts() ) : ?>

<section class="seccion-oscura text-center">
	<div class="container">
		<h2 class="display blanco">Ganadores</h1>
	</div>
</section>


<section class="ganadores">
	<div class="container">
		<div class="row">

			<div class="owl-carousel owl-theme owl-ganadores">
				<?php while ( $ganadores->have_posts() ) : $ganadores->the_post();
					$promocion 			= get_field('promocion');
					$premio 			= get_field('premio');
					$fotografia 		= get_field('fotografia');
					$fotografia_array 	= wp_get_attachment_image_src($fotografia, 'full'); ?>
				<div class="persona-ganador">
					<img src="<?php echo $fotografia_array[0];?>" class="img-responsive" alt="Ganador <?php the_title();?>" />
					<div class="nombre">
						<?php the_title();?>
					</div>
					<hr>
					<div class="promocion">
						<?php echo $promocion;?>
					</div>
					<div class="premio">
						<?php echo $premio;?>
					</div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>
</section>
<?php endif;?>

<?php get_footer();?>