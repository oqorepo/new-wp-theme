<?php
/**
* Template Name: Página Referidos
*/
get_header();
get_template_part('secciones/header-normal');
?>


<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot').addClass('activo');

// Google Tag Manager
dataLayer.push({
'tipoPagina': 'Referidos',
});



});
</script>

<?php $option = $_GET['opcion'];?>









<!-- ============================================================================= -->
<!-- Sección Título -->
<!-- ============================================================================= -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">Transforma a Tu mejor amigo en tu mejor vecino</h1>
	</div>
</section>


<div class="container">
<img src="<?php bloginfo('template_url');?>/recursos/img/amigos.jpg" class="img-responsive img-full" /
>
</div>


<section class="pagina-interior">
	<div class="container">
		<div class="col-md-4">
				
	
			<p>
			Si reservan su casa entre el 1 de Abril y el 31 de Julio, tú y tu recomendado se llevan de regalo <strong>UNA GIFTCARD DE $300.000 c/u.</strong></p>

			<img src="<?php bloginfo('template_url');?>/recursos/img/referidos.jpg" class="img-responsive" />
		</div>
		<div id="gracias-formulario-referidos" style="display:none;">
			<div class="col-md-8">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado.svg" class="img-responsive icon-enviado" alt="Enviado">
				<h3>Gracias <span class="nombre-persona"></span></h3>
				<p><strong>Tus datos han sido recibidos.</strong></p>
			</div>
		</div>
		<div id="formulario-contacto">
			<div class="col-md-8">
				<?php echo do_shortcode('
				[contact-form-7 id="1237" title="Referidos" html_id="formulario-referidos"]');?>
			</div>
		</div>
	</div>
</section>

<script>
$(document).ready(function() {
var referidos = '<?php echo $option;?>';

$('select[name="ProyectosReferidos"]').val(referidos);
});

</script>
<?php get_footer();?>