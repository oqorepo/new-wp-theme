<?php 
// Logo Personalziado

function my_login_logo() { ?>
<style type="text/css">

body {
	background:#f1f1f1 !important;
}
#login h1 a, .login h1 a {
background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/recursos/img/iconos/logo.svg);
padding-bottom: 30px;
}
.wp-core-ui .button-primary {
text-shadow:none !important;
}
.login h1 a {
margin:0 !important;
height:80px !important;
width:100% !important;
background-size:200px !important;
padding-bottom:0px !important;

border-radius: 0px !important;
}

.wp-core-ui .button-primary {
    background: #e5302d !important;
    border-color: #e5302d #e5302d #e5302d !important;
    box-shadow: 0 1px 0 #e5302d !important;
    color: #fff;
    text-decoration: none;
 
}






</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


// URL
// Agregar esta funcion a la págna cotizar

  

function add_query_vars_filter( $vars ){
  $vars[] = "proyecto";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );
// Fin URL







function pa_registrar_opciones() {
    register_setting( 'pa_opciones_theme', 'pa_opciones', 'pa_validar' );
}
 
add_action( 'admin_init', 'pa_registrar_opciones' );


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


// /* SE ELIMINAN LOS QUERY STRINGS (?) DE LOS ARCHIVOS ESTATICOS JS Y CSS*/
// function _remove_script_version( $src ){
// $parts = explode( '?', $src );
// return $parts[0];
// }
// add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
// add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
// /* FIN SE ELIMINAN LOS QUERY STRINGS (?) DE LOS ARCHIVOS ESTATICOS JS Y CSS*/


// function defer_parsing_of_js ( $url ) {
// if ( FALSE === strpos( $url, '.js' ) ) return $url;
// if ( strpos( $url, 'jquery.js' ) ) return $url;
// return "$url' defer ";
// }
// add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );


// function add_defer_to_cf7( $url )
// {
//     //If  not js file OR js file with 'jquery.ui.core.min' OR 'jquery.min' name string, then no need to apply defer
//     if(FALSE === strpos( $url, '.js') || ((strpos( $url, 'scripts') > 0) || (strpos($url, 'jquery.min') > 0))){ 
//         return $url;
//     }
//     else{
//         //set defer for .js files
//         return "$url' defer='defer";        
//     }
// }

// if( !is_admin() ){
// add_filter( 'clean_url', 'add_defer_to_cf7', 11, 1 );
// }





if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Configuraciones Avellaneda',
        'menu_title'    => 'Avellaneda',
        'menu_slug'     => 'configuraciones-avellaneda',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'icon_url'      => get_template_directory_uri() . '/recursos/img/iconos/avellaneda.png',
    
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Pendientes',
        'menu_title'    => 'Pendientes',
        'parent_slug'   => 'configuraciones-avellaneda',
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Redirecciones',
        'menu_title'    => 'Redirecciones',
        'parent_slug'   => 'configuraciones-avellaneda',
    ));
    
}

// Permite Subir archivos SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');




// Custom Excerpt function for Advanced Custom Fields
function corporativo_extracto() {
    global $post;
    $text = get_field('contenido'); //Replace 'your_field_name'
    if ( '' != $text ) {
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]&gt;', ']]&gt;', $text);
        $excerpt_length = 15; // 20 words
        $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
        $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
    }
    return apply_filters('the_excerpt', $text);
}