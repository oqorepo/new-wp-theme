<?php

// PDF Proyecto

$prefix_key = 'proyecto_';

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5a419c0ae96d4',
	'title' => 'Folleto Digital',
	'fields' => array(
		array(
			'key' => 'field_5a419c124f960',
			'label' => '',
			'name' => 'sube_el_ebook_digital',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'uploadedTo',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => 'pdf',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'proyectos',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


// Tags del Proyecto
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5a2ff2028f229',
	'title' => 'Tag del proyecto',
	'fields' => array(
		array(
			'key' => 'field_5a2fe483baff9',
			'label' => 'Selecciona tag del proyecto',
			'name' => 'selecciona_tag_del_proyecto',
			'type' => 'select',
			'instructions' => 'El tag creará un diseño llamativo en el proyecto.</br>
Por defecto se deja en normal',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'Normal' => 'Normal',
				'Entrega Inmediata' => 'Entrega Inmediata',
				'Últimas Unidades' => 'Últimas Unidades',
				'Oportunidad' => 'Oportunidad',
				'Agotado' => 'Agotado',
				'Personalizar' => 'Personalizar',
			),
			'default_value' => array(
				0 => 'Normal',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array(
			'key' => 'field_5a2fe5517be33',
			'label' => 'Personalizar Tag',
			'name' => 'personalizar_tag',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5a2fe483baff9',
						'operator' => '==',
						'value' => 'Personalizar',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'proyectos',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;