<?php
//Ubicación
function ubicaciones() {

	$labels = array(
		'name'                       => _x( 'Ubicación', 'Taxonomy General Name', 'ubicación' ),
		'singular_name'              => _x( 'Ubicación', 'Taxonomy Singular Name', 'ubicación' ),
		'menu_name'                  => __( 'Ubicaciones', 'ubicación' ),
		'all_items'                  => __( 'Todos', 'ubicación' ),
		'parent_item'                => __( 'Parent Item', 'tipo de proyecto' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ubicación' ),
		'new_item_name'              => __( 'Nuevo', 'ubicación' ),
		'add_new_item'               => __( 'Nuevo', 'ubicación' ),
		'edit_item'                  => __( 'Editar', 'ubicación' ),
		'update_item'                => __( 'Actualizar', 'ubicación' ),
		'view_item'                  => __( 'Ver', 'ubicación' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ubicación' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ubicación' ),
		'choose_from_most_used'      => __( 'Más usadas', 'ubicación' ),
		'popular_items'              => __( 'Más Usadas', 'ubicación' ),
		'search_items'               => __( 'Search Items', 'ubicación' ),
		'not_found'                  => __( 'Not Found', 'ubicación' ),
		'no_terms'                   => __( 'No items', 'ubicación' ),
		'items_list'                 => __( 'Items list', 'ubicación' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ubicación' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'ubicaciones', array( 'proyectos', 'sectores' ), $args );

}
add_action( 'init', 'ubicaciones', 0 );




// //Ubicación
// function categorias() {

// 	$labels = array(
// 		'name'                       => _x( 'Categorías', 'Taxonomy General Name', 'plantas' ),
// 		'singular_name'              => _x( 'Planta', 'Taxonomy Singular Name', 'plantas' ),
// 		'menu_name'                  => __( 'Plantas', 'plantas' ),
// 		'all_items'                  => __( 'Todos', 'plantas' ),
// 		'parent_item'                => __( 'Parent Item', 'plantas' ),
// 		'parent_item_colon'          => __( 'Parent Item:', 'plantas' ),
// 		'new_item_name'              => __( 'Nuevo', 'plantas' ),
// 		'add_new_item'               => __( 'Nuevo', 'plantas' ),
// 		'edit_item'                  => __( 'Editar', 'plantas' ),
// 		'update_item'                => __( 'Actualizar', 'plantas' ),
// 		'view_item'                  => __( 'Ver', 'plantas' ),
// 		'separate_items_with_commas' => __( 'Separate items with commas', 'plantas' ),
// 		'add_or_remove_items'        => __( 'Add or remove items', 'plantas' ),
// 		'choose_from_most_used'      => __( 'Más usadas', 'plantas' ),
// 		'popular_items'              => __( 'Más Usadas', 'plantas' ),
// 		'search_items'               => __( 'Search Items', 'plantas' ),
// 		'not_found'                  => __( 'Not Found', 'plantas' ),
// 		'no_terms'                   => __( 'No items', 'plantas' ),
// 		'items_list'                 => __( 'Items list', 'plantas' ),
// 		'items_list_navigation'      => __( 'Items list navigation', 'plantas' ),
// 	);
// 	$args = array(
// 		'labels'                     => $labels,
// 		'hierarchical'               => true,
// 		'public'                     => true,
// 		'show_ui'                    => true,
// 		'show_admin_column'          => true,
// 		'show_in_nav_menus'          => true,
// 		'show_tagcloud'              => true,
// 	);
// 	register_taxonomy( 'plantas', array( 'proyectos' ), $args );

// }
// add_action( 'init', 'plantas', 0 );


function display_taxonomy_terms($post_type, $display = false) {
    global $post;
    $term_list = wp_get_post_terms($post->ID, $post_type, array(
    	'fields' => 'names'
    ));
 
    if($display == false) {
        echo $term_list[0];
    }elseif($display == 'return') {
        return  $term_list[0];
    }
}

