<?php
// ------------------------------------------------------------------------------------ ------------------------------------------------------------------------------------

function scripts() {


// Si es página de registros
if (is_page('Registros Referidos') || is_page_template('pag-registros.php')){
wp_enqueue_script( 'DataTableSripts', get_template_directory_uri() . '/recursos/tablas.js');
wp_enqueue_script( 'DataTableJS', '//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js');
wp_enqueue_style( 'DataTableCSS', '//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' );

}



// wp_enqueue_style( 'style', get_stylesheet_uri() );

// ---------------------------------------- Bootstrap
// wp_enqueue_style( 'bootstrap-3.3.7-dist-CSS', get_template_directory_uri() .'/recursos/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css' );
// wp_enqueue_style( 'bootstrap-3.3.7-dist-CSS-Theme', get_template_directory_uri() .'/recursos/lib/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css' );
// wp_enqueue_script( 'bootstrap-3.3.7-dist-JS', get_template_directory_uri() . '/recursos/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js' );
wp_enqueue_style( 'bootstrapCSS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
wp_enqueue_script( 'bootstrapJS', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' );

// ---------------------------------------- Altura
wp_enqueue_script( 'jquery.matchHeight', get_template_directory_uri() . '/recursos/lib/jquery.matchHeight-min.js' );

// ---------------------------------------- Slick Slider
// wp_enqueue_style( 'slick-1.8.0-CSS', get_template_directory_uri() . '/recursos/lib/slick-1.8.0/slick/slick.css' );
// wp_enqueue_style( 'slick-1.8.0-Tema', get_template_directory_uri() . '/recursos/lib/slick-1.8.0/slick/slick-theme.css' );
// wp_enqueue_script( 'slick-1.8.0-JS', get_template_directory_uri() . '/recursos/lib/slick-1.8.0/slick/slick.min.js' );
// wp_enqueue_style( 'slick-1.8.0-CSS', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css' );
// wp_enqueue_style( 'slick-1.8.0-Tema', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css' );
// wp_enqueue_script( 'slick-1.8.0-JS', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js' );


wp_enqueue_script( 'LazyYTJS', get_template_directory_uri() . '/recursos/lib/LazyYT/lazyYT.min.js' );
wp_enqueue_style( 'LazyYTCSS', get_template_directory_uri() . '/recursos/lib/LazyYT/lazyYT.min.css' );

wp_enqueue_style( 'OwlCarouselCSS', get_template_directory_uri() . '/recursos/lib/OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css' );
wp_enqueue_style( 'OwlCarouselTheme', get_template_directory_uri() . '/recursos/lib/OwlCarousel2-2.2.1/dist/assets/owl.theme.default.min.css' );
wp_enqueue_script( 'OwlCarouselJS', get_template_directory_uri() . '/recursos/lib/OwlCarousel2-2.2.1/dist/owl.carousel.min.js' );
// ---------------------------------------- Filtros Isotope
if(is_singular('proyectos')){
// wp_enqueue_script( 'isotope', get_template_directory_uri() . '/recursos/lib/isotope.pkgd.min.js' );
wp_enqueue_script( 'isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.5/isotope.pkgd.min.js' );
wp_enqueue_script( 'isotope-loading', get_template_directory_uri() . '/recursos/lib/imagesloaded.pkgd.min.js');

wp_enqueue_script( 'scripts-proyectos', get_template_directory_uri() . '/recursos/
	scripts-single-proyectos.js');
}




if(is_page('Servicio Post Venta')){

wp_enqueue_script( 'iFrame.js', get_template_directory_uri() . '/recursos/iframe.js');

}
// ---------------------------------------- Zoom en Imagen
wp_enqueue_script( 'zooming', get_template_directory_uri() . '/recursos/lib/zooming-1.2.6/build/zooming.min.js' );

wp_enqueue_script( 'FancyBox3JS', get_template_directory_uri() . '/recursos/lib/fancybox-master/dist/jquery.fancybox.min.js' );
wp_enqueue_style( 'FancyBox3CSS', get_template_directory_uri() . '/recursos/lib/fancybox-master/dist/jquery.fancybox.min.css' );


// ---------------------------------------- Mmenu App Like
wp_enqueue_script( 'mmenu.js', get_template_directory_uri() . '/recursos/lib/jquery.mmenu/jquery.mmenu.all.js' );
wp_enqueue_style( 'mmenu.CSS', get_template_directory_uri() . '/recursos/lib/jquery.mmenu/jquery.mmenu.all.css' );

// wp_enqueue_script( 'mmenu.js', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.0/jquery.mmenu.all.js' );
// wp_enqueue_style( 'mmenu.CSS', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.0/jquery.mmenu.css' );


// ---------------------------------------- Tooltipster
wp_enqueue_script( 'tooltipster-JS', get_template_directory_uri() . '/recursos/lib/tooltipster-master/dist/js/tooltipster.bundle.min.js' );
wp_enqueue_style( 'tooltipster-CSS', get_template_directory_uri() . '/recursos/lib/tooltipster-master/dist/css/tooltipster.bundle.min.css' );
wp_enqueue_style( 'tooltipster-Tema', get_template_directory_uri() . '/recursos/lib/tooltipster-master/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css' );
wp_enqueue_style( 'followtooltip-css', get_template_directory_uri() . '/recursos/lib/tooltipster-master/follow/css/tooltipster-follower.min.css' );
wp_enqueue_script( 'followtooltip-JS', get_template_directory_uri() . '/recursos/lib/tooltipster-master/follow/js/tooltipster-follower.min.js' );
// wp_enqueue_script( 'tooltipster-JS', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js' );
// wp_enqueue_style( 'tooltipster-CSS', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css');
// wp_enqueue_style( 'tooltipster-Tema', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-shadow.min.css' );
// wp_enqueue_style( 'followtooltip-css', 'https://cdn.jsdelivr.net/npm/tooltipster-follower@0.1.5/dist/css/tooltipster-follower.min.css' );
// wp_enqueue_script( 'followtooltip-JS', 'https://cdn.jsdelivr.net/npm/tooltipster-follower@0.1.5/dist/js/tooltipster-follower.min.js' );

// ---------------------------------------- Animate
// wp_enqueue_style( 'animate', get_template_directory_uri() . '/recursos/lib/animate.css' );
wp_enqueue_style( 'animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' );


// ---------------------------------------- Validación de Formularios
if(is_singular('proyectos') || is_singular('plantas') || is_page('Contacto') || is_page('Servicio Post Venta') || is_page('Cotizar') || is_singular('landing') || is_page('referidos') ){
wp_enqueue_script( 'jquery-validation', get_template_directory_uri() . '/recursos/lib/jquery-validation-1.17.0/dist/jquery.validate.min.js');
wp_enqueue_script( 'jquery-validation-adicional', get_template_directory_uri() . '/recursos/lib/jquery-validation-1.17.0/dist/additional-methods.min.js');
wp_enqueue_script( 'Rut', get_template_directory_uri() . '/recursos/lib/rut.js');
// ---------------------------------------- Checkbox

wp_enqueue_script( 'iCheckJS', get_template_directory_uri() . '/recursos/lib/icheck-1.x/icheck.min.js');

wp_enqueue_style( 'iCheckCSS', get_template_directory_uri() . '/recursos/lib/icheck-1.x/skins/flat/red.css');


// ---------------------------------------- Formularios
wp_enqueue_script( 'formularios', get_template_directory_uri().'/recursos/formularios.js', array( 'jquery'  ) );
wp_localize_script( 'formularios', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')) );



}




wp_enqueue_script( 'scrollReveal', get_template_directory_uri() . '/recursos/lib/scrollreveal-master/dist/scrollreveal.min.js');




// ---------------------------------------- Font Awesome
wp_enqueue_script( 'FontAwesome', 'https://use.fontawesome.com/releases/v5.0.4/js/all.js' );

// ---------------------------------------- JS Cookie
wp_enqueue_script( 'Cookie', get_template_directory_uri() . '/recursos/lib/js.cookie.js');




// ---------------------------------------- Tipografía
wp_enqueue_style( 'Typo', '//fonts.googleapis.com/css?family=Montserrat:400,600' );
// ---------------------------------------- Scripts y Estilo
wp_enqueue_script( 'scripts', get_template_directory_uri() . '/recursos/scripts.js' );

wp_enqueue_script( 'slides', get_template_directory_uri() . '/recursos/js/slides.js' );


if (is_front_page()){
wp_enqueue_script( 'pag-inicio', get_template_directory_uri() . '/recursos/js/pag-inicio.js' );

}
wp_enqueue_style( 'Estilo', get_template_directory_uri() .'/recursos/style.css' );
wp_enqueue_style( 'Responsive', get_template_directory_uri() .'/recursos/responsive.css' );
// wp_enqueue_style( 'Animaciones', get_template_directory_uri() .'/recursos/animaciones.css' );

}
add_action( 'wp_enqueue_scripts', 'scripts' );


  

// ---------------------------------------- jQuery Op
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
wp_deregister_script('jquery');
wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js", false, null);
wp_enqueue_script('jquery');



}

// Op Playlist: https://soundcloud.com/unboxmusic
// Miércoles 6 de Diciembre 2017
// hola@jorge.life


add_filter( 'script_loader_tag', 'wsds_defer_scripts', 10, 3 );
function wsds_defer_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 
		'scripts',
		'admin-bar',
		'et_monarch-ouibounce',
		'et_monarch-custom-js',
		'wpshout-js-cookie-demo',
		'cookie',
		'wpshout-no-broken-image',
		'goodbye-captcha-public-script',
		'devicepx',
		'search-box-value',
		'page-min-height',
		'kamn-js-widget-easy-twitter-feed-widget',
		'__ytprefs__',
		'__ytprefsfitvids__',
		'jquery-migrate',
		'icegram',
		'disqus',
	);

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }
    
    return $tag;
} 