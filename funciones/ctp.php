<?php
// PROYECTOS


add_action( 'init', 'register_cpt_proyectos' );

function register_cpt_proyectos() {

	$labels = array(
		'name' => __( 'Proyectos', 'proyectos' ),
		'singular_name' => __( 'Proyecto', 'proyectos' ),
		'add_new' => __( 'Agregar', 'proyectos' ),
		'add_new_item' => __( 'Agregar', 'proyectos' ),
		'edit_item' => __( 'Editar', 'proyectos' ),
		'new_item' => __( 'Nuevo', 'proyectos' ),
		'view_item' => __( 'Ver', 'proyectos' ),
		'search_items' => __( 'Buscar', 'proyectos' ),
		'not_found' => __( 'Sin Resultados', 'proyectos' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'proyectos' ),
		'parent_item_colon' => __( 'Proyectos', 'proyectos' ),
		'menu_name' => __( 'Proyectos', 'proyectos' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'taxonomies' => array( 'Región' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title', 'revisions' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/proyectos.png'
	);

	register_post_type( 'proyectos', $args );
}







add_action( 'init', 'register_cpt_corporativo' );

function register_cpt_corporativo() {

	$labels = array(
		'name' => __( 'Corporativo', 'corporativo' ),
		'singular_name' => __( 'Corporativo', 'corporativo' ),
		'add_new' => __( 'Agregar', 'corporativo' ),
		'add_new_item' => __( 'Agregar', 'corporativo' ),
		'edit_item' => __( 'Editar', 'corporativo' ),
		'new_item' => __( 'Nuevo', 'corporativo' ),
		'view_item' => __( 'Ver', 'corporativo' ),
		'search_items' => __( 'Buscar', 'corporativo' ),
		'not_found' => __( 'Sin Resultados', 'corporativo' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'corporativo' ),
		'parent_item_colon' => __( 'Proyectos', 'corporativo' ),
		'menu_name' => __( 'Corporativo', 'corporativo' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/avellaneda.png'
	);

	register_post_type( 'corporativo', $args );
}



// Plantas
add_action( 'init', 'register_cpt_plantas' );

function register_cpt_plantas() {

	$labels = array(
		'name' => __( 'Plantas', 'plantas' ),
		'singular_name' => __( 'Planta', 'plantas' ),
		'add_new' => __( 'Agregar', 'plantas' ),
		'add_new_item' => __( 'Agregar', 'plantas' ),
		'edit_item' => __( 'Editar', 'plantas' ),
		'new_item' => __( 'Nuevo', 'plantas' ),
		'view_item' => __( 'Ver', 'plantas' ),
		'search_items' => __( 'Buscar', 'plantas' ),
		'not_found' => __( 'Sin Resultados', 'plantas' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'plantas' ),
		'parent_item_colon' => __( 'Proyectos', 'plantas' ),
		'menu_name' => __( 'Plantas', 'plantas' ),
	
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'revisions' ),
		// 'taxonomies' => array( 'Región' ),
		'public' => true,
		'show_ui' => true,
		'menu_position' => 5,
		'publicly_queryable' => true,
			    'show_in_menu' => 'edit.php?post_type=proyectos',
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => true,
		// // 'rewrite'            => array( 'slug' => 'plantas' ),
		//  "rewrite" => array( "slug" => "plantas", "with_front" => true ),
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/proyectos.png',

 // 'rewrite' => array(
 //    'slug' => '/proyectos/condominio-plaza-buin',
 //    'with_front' => true
 //  )



	);

	register_post_type( 'plantas', $args );
}






// Promociones y Publicidad

add_action( 'init', 'register_cpt_promociones' );

function register_cpt_promociones() {

	$labels = array(
		'name' => __( 'Promociones', 'promociones' ),
		'singular_name' => __( 'Promoción', 'promociones' ),
		'add_new' => __( 'Agregar', 'promociones' ),
		'add_new_item' => __( 'Agregar', 'promociones' ),
		'edit_item' => __( 'Editar', 'promociones' ),
		'new_item' => __( 'Nuevo', 'promociones' ),
		'view_item' => __( 'Ver', 'promociones' ),
		'search_items' => __( 'Buscar', 'promociones' ),
		'not_found' => __( 'Sin Resultados', 'promociones' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'promociones' ),
		'parent_item_colon' => __( 'Promociones', 'promociones' ),
		'menu_name' => __( 'Promociones', 'promociones' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/promociones.png'
	);

	register_post_type( 'promociones', $args );
}

// Comunicados

add_action( 'init', 'register_cpt_comunicados' );

function register_cpt_comunicados() {

	$labels = array(
		'name' => __( 'Comunicados', 'comunicados' ),
		'singular_name' => __( 'Comunicados', 'comunicados' ),
		'add_new' => __( 'Agregar', 'comunicados' ),
		'add_new_item' => __( 'Agregar', 'comunicados' ),
		'edit_item' => __( 'Editar', 'comunicados' ),
		'new_item' => __( 'Nuevo', 'comunicados' ),
		'view_item' => __( 'Ver', 'comunicados' ),
		'search_items' => __( 'Buscar', 'comunicados' ),
		'not_found' => __( 'Sin Resultados', 'comunicados' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'comunicados' ),
		'parent_item_colon' => __( 'Comunicados', 'comunicados' ),
		'menu_name' => __( 'Comunicados', 'comunicados' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/comunicados.png'
	);

	register_post_type( 'comunicados', $args );
}

add_action( 'init', 'register_cpt_landing' );

function register_cpt_landing() {

	$labels = array(
		'name' => __( 'Landings', 'landing' ),
		'singular_name' => __( 'Landings', 'landing' ),
		'add_new' => __( 'Agregar', 'landing' ),
		'add_new_item' => __( 'Agregar', 'landing' ),
		'edit_item' => __( 'Editar', 'landing' ),
		'new_item' => __( 'Nuevo', 'landing' ),
		'view_item' => __( 'Ver', 'landing' ),
		'search_items' => __( 'Buscar', 'landing' ),
		'not_found' => __( 'Sin Resultados', 'landing' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'landing' ),
		'parent_item_colon' => __( 'Landings', 'landing' ),
		'menu_name' => __( 'Landings', 'landing' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'taxonomies'  => array( 'category' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/landing.png'
	);

	register_post_type( 'landing', $args );
}





add_action( 'init', 'register_cpt_sectores' );

function register_cpt_sectores() {

	$labels = array(
		'name' => __( 'Sectores', 'sectores' ),
		'singular_name' => __( 'Sectores', 'sectores' ),
		'add_new' => __( 'Agregar', 'sectores' ),
		'add_new_item' => __( 'Agregar', 'sectores' ),
		'edit_item' => __( 'Editar', 'sectores' ),
		'new_item' => __( 'Nuevo', 'sectores' ),
		'view_item' => __( 'Ver', 'sectores' ),
		'search_items' => __( 'Buscar', 'sectores' ),
		'not_found' => __( 'Sin Resultados', 'sectores' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'sectores' ),
		'parent_item_colon' => __( 'Sectores', 'sectores' ),
		'menu_name' => __( 'Sectores', 'sectores' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/sectores.png'
	);

	register_post_type( 'sectores', $args );
}


/*  add_action( 'init', 'register_cpt_legal' );

function register_cpt_legal() {

	$labels = array(
		'name' => __( 'Legal', 'legal' ),
		'singular_name' => __( 'Legal', 'legal' ),
		'add_new' => __( 'Agregar', 'legal' ),
		'add_new_item' => __( 'Agregar', 'legal' ),
		'edit_item' => __( 'Editar', 'legal' ),
		'new_item' => __( 'Nuevo', 'legal' ),
		'view_item' => __( 'Ver', 'legal' ),
		'search_items' => __( 'Buscar', 'legal' ),
		'not_found' => __( 'Sin Resultados', 'legal' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'legal' ),
		'parent_item_colon' => __( 'Legal', 'legal' ),
		'menu_name' => __( 'Legal', 'legal' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'capabilities' => array(
			'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
		),
		'map_meta_cap' => true,
		'menu_icon' => 'dashicons-shield-alt'
	);

	register_post_type( 'legal', $args );
}  */

// add_filter( 'wpseo_enable_xml_sitemap_transient_caching', '__return_false' );





function my_permalinks($permalink, $post, $leavename) {
	$post_id = $post->ID;
	if($post->post_type != 'plantas' || empty($permalink) || in_array($post->post_status, array('draft', 'pending', 'auto-draft')))
	 	return $permalink;
	$parent = get_field('vincular_planta_a_proyecto');
	// $parent = $post->post_parent;
	$parent_post = get_post( $parent );
	$permalink = str_replace('%proyectos%', $parent_post->post_name, $permalink);
	return $permalink;
}
add_filter('post_type_link', 'my_permalinks', 10, 3);



// Crear URL personalizada en Plantas
function my_add_rewrite_rules() {
	add_rewrite_tag('%plantas%', '([^/]+)', 'plantas=');
	add_permastruct('plantas', '/proyectos/%proyectos%/plantas/%plantas%', true);
	add_rewrite_rule('^proyectos/([^/]+)/plantas/([^/]+)/?','index.php?plantas=$matches[2]','top');


}
add_action( 'init', 'my_add_rewrite_rules' );










// Agrega título del proyecto cuando se vincula una planta a un proyecto
// para SEO
function custom_post_type_title ( $post_id ) {
    global $wpdb;
    if ( get_post_type( $post_id ) == 'plantas' ) {
        // $engine= ', '.get_post_meta($post_id, 'Engine', true).'l';
        // $terms = wp_get_object_terms($post_id, 'brand');
        // $abrand= ' '.$terms[0]->name;
        // $amodel = ' '.$terms[1]->name;
        // $title = $post_id.$abrand.$amodel.$engine;
        // $where = array( 'ID' => $post_id );
        // $wpdb->update( $wpdb->posts, array( 'post_title' => $title ), $where );
    }
}
add_action( 'save_post', 'custom_post_type_title' );


add_action( 'init', 'register_cpt_logos' );

function register_cpt_logos() {

	$labels = array(
		'name' => __( 'Logos', 'logos' ),
		'singular_name' => __( 'Logos', 'logos' ),
		'add_new' => __( 'Agregar', 'logos' ),
		'add_new_item' => __( 'Agregar', 'logos' ),
		'edit_item' => __( 'Editar', 'logos' ),
		'new_item' => __( 'Nuevo', 'logos' ),
		'view_item' => __( 'Ver', 'logos' ),
		'search_items' => __( 'Buscar', 'logos' ),
		'not_found' => __( 'Sin Resultados', 'logos' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'logos' ),
		'parent_item_colon' => __( 'Logos', 'logos' ),
		'menu_name' => __( 'Logos', 'logos' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => array( 'revisions' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/logos.png'
	);

	register_post_type( 'logos', $args );
}



add_action( 'init', 'register_cpt_ganadores' );

function register_cpt_ganadores() {

	$labels = array(
		'name' => __( 'Ganadores', 'ganadores' ),
		'singular_name' => __( 'Ganadores', 'ganadores' ),
		'add_new' => __( 'Agregar', 'ganadores' ),
		'add_new_item' => __( 'Agregar', 'ganadores' ),
		'edit_item' => __( 'Editar', 'ganadores' ),
		'new_item' => __( 'Nuevo', 'ganadores' ),
		'view_item' => __( 'Ver', 'ganadores' ),
		'search_items' => __( 'Buscar', 'ganadores' ),
		'not_found' => __( 'Sin Resultados', 'ganadores' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'ganadores' ),
		'parent_item_colon' => __( 'Ganadores', 'ganadores' ),
		'menu_name' => __( 'Ganadores', 'ganadores' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'supports' => array( 'title' ),
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => get_template_directory_uri() . '/recursos/img/iconos/ganadores.png'
	);

	register_post_type( 'ganadores', $args );
}

