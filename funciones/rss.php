<?php

// RSS Proyectos

add_action('init', 'rssProyectos');
function rssProyectos(){
        add_feed('feedproyectos', 'rssProyectosFuncion');
}


function rssProyectosFuncion(){
        get_template_part('rss', 'feedproyectos');
}

// RSS Plantas
add_action('init', 'rssPlantas');
function rssPlantas(){
        add_feed('feedplantas', 'rssPlantasFuncion');
}


function rssPlantasFuncion(){
        get_template_part('rss', 'feedplantas');
}