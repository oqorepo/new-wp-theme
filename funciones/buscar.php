<?php
// add the ajax fetch js
add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
function fetch(){

    $.ajax({
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        type: 'post',
        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
        success: function(data) {


            $('#datafetch').html( data );
        }
    });






}
</script>

<?php
}

// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

    $the_query = new WP_Query( array( 
    'posts_per_page' => -1,
    's' => esc_attr( $_POST['keyword'] ),
    'post_type' => 'proyectos',
    'taxonomy'      => array( 'ubicaciones' ), // taxonomy name


     ) );
    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post(); ?>

            <p><a href="<?php echo esc_url( post_permalink() ); ?>"><?php the_title();?></a></p>

        <?php endwhile;?>

    <?php else:?>
    	Nada
    	<?php
        wp_reset_postdata();  
    endif;

    die();
}