<?php
add_theme_support( 'post-thumbnails' );




add_image_size('fotografias-medio', 700, false);
add_image_size('fotografias-chica', 300, false);


// Tamaño Original
// Tamaño responsive
add_image_size('fotografias', 1200, 550, true);
add_image_size('fotografias-equipamiento', 500, 229, true);


// Tamaño fotografías para el menú
add_image_size('foto-menu-col2', 165, 76, true); //col-md-2
add_image_size('foto-menu-col3', 262, 120, true); //col-md-3
add_image_size('foto-menu-col4', 360, 165, true); // col-md-4

// Tamaño fotografías casas en listado de plantas
add_image_size('fotografias-casas', 400, 267);

// Tamaño fotografías departamentos en listado de plantas
add_image_size('fotografias-departamentos', 400, 400);

// Tamaño de fotografías tab-mobile
add_image_size('fotografias-tab-cuadrado', 400, 400, true);

// Experiencia Avellaneda
add_image_size('experiencia', 500, 282);

add_image_size('corporativo', 780, 439, true);

//  Esquicios
add_image_size('esquicios', 500, 239, true);