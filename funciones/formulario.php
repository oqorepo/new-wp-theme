<?php
add_filter( 'wpcf7_validate_configuration', '__return_false' );
   


add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');

function my_wpcf7_ajax_loader () {
    return  get_template_directory_uri() . '/recursos/img/iconos/loading.svg';
}

function reformat_auto_p_tags($content) {
    $new_content = '';
    $pattern_full = '{(\[raw\].*?\[/raw\])}is';
    $pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'reformat_auto_p_tags', 99);
add_filter('widget_text', 'reformat_auto_p_tags', 99);



// // Carga Contact Form 7, sólo en las páginas que tengan formulario

// add_filter( 'wpcf7_load_js', '__return_false' );
// add_filter( 'wpcf7_load_css', '__return_false' );




if (is_singular('plantas') || is_singular('proyectos') || is_page('Contacto') || is_page('Servicio Post Venta')){

      if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
 
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }
}



add_action( 'wp_print_styles', 'aa_deregister_styles', 100 );
function aa_deregister_styles() {
    if ( ! is_page( 'Contacto' ) || ! is_singular('proyectos') || ! is_singular('plantas') || !is_page('Servicio Post-venta') || !is_page('Cotizar')) {
        wp_deregister_style( 'contact-form-7' );
    }
}

