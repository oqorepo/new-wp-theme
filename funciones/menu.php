<?php
function register_my_menus() {
  register_nav_menus(
    array(
      'top-menu' => __( 'Menu Arriba' ),
      'principal-menu' => __( 'Menu Principal' ),
      'responsive-menu' => __( 'Menu Celulares' )
    )
  );
}
add_action( 'init', 'register_my_menus' );



function wp_nav_menu_no_ul()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'principal-menu',
        

    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}

function wp_nav_menu_no_ul_celular()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'responsive-menu'

    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}
