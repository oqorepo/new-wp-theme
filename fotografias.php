<?php
add_theme_support( 'post-thumbnails' );




add_image_size('fotografias-medio', 700, false);
add_image_size('fotografias-chica', 300, false);


// Tamaño Original
// Tamaño responsive
add_image_size('fotografias', 1200,550, true);
add_image_size('fotografias-equipamiento', 500,229, true);


// Tamaño fotografías para el menú
add_image_size('foto-menu', 300,138, true);


// Tamaño fotografías casas en listado de plantas
add_image_size('fotografias-casas', 400, 267);

// Tamaño fotografías departamentos en listado de plantas
add_image_size('fotografias-departamentos', 400, 400);


// Experiencia Avellaneda
add_image_size('experiencia', 500, 282);