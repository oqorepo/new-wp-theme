<?php
/**
* Template Name: Página Oportunidades
*/
get_header();
?>

<script>
$(document).ready(function() {
$('.head').css('border', '0px');
// Google Tag Manager
dataLayer.push({
'tipoPagina': '<?php the_title();?>',
});
});
</script>

<?php get_template_part('secciones/header-normal');?>

<?php
$terms = get_the_terms( $post->ID , 'ubicaciones' );
if ( $terms != null ):?>
<?php
foreach( $terms as $term ) :?>
<?php if ($term->parent == "23"):?>
<script>
$('.head').css('border-bottom', '0px');
</script>
<?php elseif ($term->parent == "25"):?>
<script>
$('.head').css('border-bottom', '0px');
</script>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>






<!-- ============================================================================= -->
<!-- Sección Título -->
<!-- ============================================================================= -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">Oportunidades</h1>
	</div>
</section>


<!-- ============================================================================= -->
<!-- Sección Proyectos -->
<!-- ============================================================================= -->




<div class="separador-de-seccion" style="padding-top:30px;">	
<div class="container">
	<div class="row">
			<div class="row">
<?php $ids = get_field('seleccionar_proyectos', false, false);?>

		<?php
		$_posts = new WP_Query( array(
		'post_type'         => 'proyectos',
		'posts_per_page'    => -1,
		'post__in'			=> $ids,
	'post_status'		=> 'publish',
	'orderby'        	=> 'post__in',
));
		if( $_posts->have_posts() ) :
		while ( $_posts->have_posts() ) : $_posts->the_post();
		$pronto = get_field('url_landing_proyecto');
		$fotografia_menu = get_field('foto_portada_para_menu');
		$size = 'foto-menu-col4';
		$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
		?>
		


<?php if ($pronto):?>
	<a href="<?php echo $pronto;?>" class="tooltipster" title="Ver Proyecto" target="_new"/>
<?php else:?>

	<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" />


		<div class="col-md-4 bloque-proyecto-menu" style="margin-bottom:15px;">
			
			<?php
			$fotografia = get_field('foto_portada_para_menu');
				$size = 'foto-menu-col4';
				$image_array = wp_get_attachment_image_src($fotografia, $size);
			
			?>

        



	<div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;">
		<span style="font-size:10px !important; top:42px;">
			<?php
							// variables
							$tag = get_field('selecciona_tag_del_proyecto');
							$tag_personalizado = get_field('personalizar_tag');
			if ($tag == "Personalizar"):?>
			<?php echo $tag_personalizado;?>
			<?php elseif ($tag == "Normal"):?>
			<?php else:?>
			<?php echo $tag;?>
			<?php endif;?>
		</span>
	</div>


	
				
					<?php if ($fotografia):?>
					<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="Proyecto <?php the_title();?>" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full" alt="Sin Fotografía"/>
					<?php endif;?>
					
					
			<div class="listado_de_proyectos altura-proyectos-buscador">
				<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
				<h4><?php the_title(); ?></h4>
				<hr></hr>
				<p><?php the_field('bloque_rojo');?></p>
				
				
			</div>

			</div>

</a>
		<?php endif;?>

			<?php
			endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</div>

</div>
</div>



<!-- ============================================================================= -->
<!-- Sección Título -->
<!-- ============================================================================= -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">Promociones</h1>
	</div>
</section>




<!-- Loop Promociones -->
<?php
$post_id          = get_the_ID();
$hoy              = date( 'Ymd', current_time( 'timestamp', 0 ));
$query            = new WP_Query(array(
'post_type'       => array('promociones'),
'posts_per_page'  => -1,
'post_status'     => 'publish',
'order'           => 'DESC',
'orderby'         => 'meta_value',
'meta_key'        => 'fecha_de_expiracion' // Se ordena por fecha de expiración
));
if ( $query->have_posts() ) : ?>
<section class="mas-promos">
  <div class="container">
    <div class="row">
      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="col-md-4" style="margin-top:30px;">
        <?php // Variables
        $imagen = get_field('imagen_promocion_mobile');
        $imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
        $estado = get_field('estado');
        $personalizar = get_field('personalizar');
        $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        $fechaFormato = $fecha->format('Ymd');
        $hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
        $dateformatstring = "j F Y";
        $unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
        $selecciona = get_field('selecciona_proyecto_o_planta');
        ?>
        <!-- Condicionales -->
        <?php  if ($fechaFormato >= $hoy):?>
        <a href="<?php the_permalink();?>" id="clickPromocion" onclick="dataLayer.push({'Promo': '<?php the_title();?>'});">
          <div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;">
            <span style="font-size:13px !important; top:42px;">
              <?php if ($estado == "Personalizar"):?>
              <?php echo $personalizar;?>
              <?php else:?>
              <?php echo $estado;?>
              <?php endif;?>
            </span>
          </div>
          <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster" title="Ver Promoción"  alt="<?php the_title();?>" />
        </a>
        <strong><?php the_title();?></strong>
        <p><i class="far fa-calendar-alt"></i> Vigencia hasta <?php echo date_i18n($dateformatstring, $unixtimestamp);?> </p>
        <hr>
        <?php if( $selecciona ): ?>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php else:?>
        <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster blancoynegro" title="Promoción No Vigente"  alt="<?php the_title();?>" />
        <strong><?php the_title();?></strong>
        <div><i class="far fa-calendar-alt"></i> Promoción No Vigente</div>
        Finalizó el <?php echo date_i18n($dateformatstring, $unixtimestamp);?>
        <?php if( $selecciona ): ?>
        <hr>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php endif;?>
        <!-- Condicionales -->
      </div>
      <?php endwhile;?>
    </div>
  </div>
</section>
<?php endif;?>

<?php get_footer();?>
