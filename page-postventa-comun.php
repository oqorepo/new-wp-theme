<?php
/**
 * Template Name: Servicio Post-Venta-común
*/
get_header(); 
get_template_part('secciones/header-normal');
?>



<section class="pagina-interior">

<div class="container">
    <ul class="breadcrumb">
		<li>
			<a href="<?php bloginfo('url');?>">Inicio</a>
		</li>
		<?php if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)):?>

		<?php else:?>
		<li>
			<?php echo get_the_title($parent); ?>
		</li>
		<?php endif;?>

		<li class="active">
			<?php the_title();?>
		</li>
	</ul>
	<div class="row">
		<div class="text-center" style="background:#f4f4f4;padding:10px;margin-bottom:40px;">
		<img src="<?php bloginfo('template_url');?>/recursos/img/siren.png" alt="Emergencia servio postventa Avellaneda" style="padding:10px;">
			<h4><b>EMERGENCIA DE SERVICIOS (Agua, Luz, Gas)</b></h4>
			<p>Si tienes una emergencia de servicios, por favor contacta directamente a la empresa <a href="https://www.avellaneda.cl/servicios-basicos-proyectos/" target="_blank">puedes revisar aquí. <button>Ir</button></a>	</p>
			
		</div>
	</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php the_content();?>

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div>

</section>


<?php get_footer();?>