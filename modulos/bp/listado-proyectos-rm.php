<!-- Muestra listado de proyectos por comuna y su interior -->


<?php
	$_terms = $terms = get_terms( 'ubicaciones', array(
			'hide_empty' => true,
			'parent' => '23',
			) );
foreach ($_terms as $term) :?>
<?php
$nombre = $term->name;
$slug = $term->slug;




?>
<div class="btn btn-lg btn-opciones btn-rojo btn-gris btn-<?php echo $slug;?>">
	<?php echo $nombre; ?> 
</div>

<script>
$('.btn-<?php echo $slug;?>').on('click', function(){




$('.btn-gris').removeClass('btn-activo');
$(this).addClass('btn-activo');
$('.resultado-listado').hide();
$('.resultado-<?php echo $slug;?>').fadeIn();
});
$('.btn-volver').on('click', function(){
$('.resultado-<?php echo $slug;?>').hide();
});
</script>
<?php
$_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
			'tax_query' => array(
			array(
			'taxonomy' => 'ubicaciones',
			'field'    => 'slug',
			'terms'    => $slug,
			'parent'   => '23',
			),
			),
			));
			if( $_posts->have_posts() ) :?>


<div class="resultados-listado-proyecto">
	<?php
			while ( $_posts->have_posts() ) : $_posts->the_post();
			$pronto = get_field('url_landing_proyecto');
			?>


<?php if ($pronto):?>




<div class="col-md-3 resultado-listado resultado-<?php echo $slug;?>">
				<div class="estado-proyecto-small-menu">
					
					
					<?php
					// variables
					$tag = get_field('selecciona_tag_del_proyecto');
					$tag_personalizado = get_field('personalizar_tag');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					
				</div>
		
				<?php

				$fotografia = get_field('foto_portada_para_menu');
					$size = 'menu';
					$image_array = wp_get_attachment_image_src($fotografia, $size);
					$image_url = $image_array[0];
				?>
				<a href="<?php echo $pronto;?>" target="_new" class="tooltipster" title="Ver Proyecto">
					<img src="<?php echo $image_url;?>" class="img-responsive" />
		
					<div class="listado_de_proyectos">
						<h5><?php echo $term->name;?></h5>
						<h4><?php the_title(); ?></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
<!-- 
						<div class="btn btn-lg btn-opciones btn-rojo">Ver proyecto</div> -->
						
					</div>
				</a>
			</div>








<?php else:?>
	<div class="col-md-3 resultado-listado resultado-<?php echo $slug;?>">
				<div class="estado-proyecto-small-menu">
					
					
					<?php
					// variables
					$tag = get_field('selecciona_tag_del_proyecto');
					$tag_personalizado = get_field('personalizar_tag');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					
				</div>
			<?php
				$fotografia = get_field('foto_portada_para_menu');
					$size = 'menu';
					$image_array = wp_get_attachment_image_src($fotografia, $size);
					$image_url = $image_array[0];
				?>
				<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto">
					<img src="<?php echo $image_url;?>" class="img-responsive" />
				
					<div class="listado_de_proyectos">
						<h5><?php echo $term->name;?></h5>
						<h4><?php the_title(); ?></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>

						
						
					</div>
				</a>
			</div>



<?php endif;?>

<?php endwhile;?>
</div>
<?php endif;?>
<?php 	wp_reset_postdata();?>

<?php endforeach;?>



<!-- Cierre de listado -->