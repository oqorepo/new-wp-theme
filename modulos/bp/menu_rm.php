<div class="proyectos_rm">
	<div class="container">
		<div class="row">
			<?php
			$_terms = $terms = get_terms( 'ubicaciones', array(
			'hide_empty' => true,
			'parent' => '23',
			) );
			foreach ($_terms as $term) :
			$term_slug = $term->slug;
			$_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
			'orderby' => 'title',
    'order'   => 'DESC',
			'tax_query' => array(
			array(
			'taxonomy' => 'ubicaciones',
			'field'    => 'slug',
			'terms'    => $term_slug,
			'parent'   => '23',
			),
			),
			));
			if( $_posts->have_posts() ) :
			while ( $_posts->have_posts() ) : $_posts->the_post();
			$pronto = get_field('url_landing_proyecto');
			
			$foto_menu = get_field('foto_portada_para_menu');
			$size = 'foto-menu';
			$image_array = wp_get_attachment_image_src($foto_menu, $size);
			?>
			<?php if ($pronto):?>
			<a href="<?php echo $pronto;?>" target="_new" class="tooltipster" title="Ver Proyecto" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
				<div class="col-md-2 bloque-proyecto-menu">
					<div class="estado-proyecto-small-menu">
						<?php
						// variables
						$tag = get_field('selecciona_tag_del_proyecto');
						$tag_personalizado = get_field('personalizar_tag');
						if ($tag == "Personalizar"):?>
						<span class="label label-default"><?php echo $tag_personalizado;?></span>
						<?php elseif ($tag == "Normal"):?>
						<?php else:?>
						<span class="label label-default"><?php echo $tag;?></span>
						<?php endif;?>
						
					</div>
					<?php if ($foto_menu):?>
					<img src="<?php echo $image_array[0];?>" class="img-responsive" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive"/>
					<?php endif;?>
					<div class="listado_de_proyectos">
						<h5><?php echo $term->name;?></h5>
						<h4><?php the_title(); ?></h4>
						
						
					</div>
				</div>
			</a>
			<?php else:?>
			<div class="col-md-2 bloque-proyecto-menu">
				
				<?php
				$fotografia = get_field('foto_portada_para_menu');
					$size = 'foto-menu';
					$image_array = wp_get_attachment_image_src($fotografia, $size);
					$image_url = $image_array[0];
				?>
				<div class="estado-proyecto-small-menu">
					
					
					<?php
					// variables
					$tag = get_field('selecciona_tag_del_proyecto');
					$tag_personalizado = get_field('personalizar_tag');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					
				</div>
				
				<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
					<?php if ($fotografia):?>
					<img src="<?php echo $image_url;?>" class="img-responsive" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive"/>
					<?php endif;?>
					
					<div class="listado_de_proyectos">
						<h5><?php echo $term->name;?></h5>
						<h4><?php the_title(); ?></h4>
						
					</div>
				</a>
			</div>
			
			<?php endif;?>
			<?php
			endwhile;
			endif;
			wp_reset_postdata();
			endforeach;
			?>
		</div>
	</div>
</div>