<div class="proyectos_rm">
	<div class="container">
		<div class="row">
			<?php
			$_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
						'post_status'		=> 'publish',
				'tax_query' => array(
				array(
				'taxonomy' => 'ubicaciones',
				'terms'    => '23'),
				),
			));
			if( $_posts->have_posts() ) :
			while ( $_posts->have_posts() ) : $_posts->the_post();
								$pronto 					= get_field('url_landing_proyecto');
						$fotografia_menu 			= get_field('foto_portada_para_menu');
									$size 						= 'foto-menu-col2';
							$image_array 				= wp_get_attachment_image_src($fotografia_menu, $size);
			?>
			
			<div class="col-md-2 bloque-proyecto-menu">
				
				<?php
							$fotografia 			= get_field('foto_portada_para_menu');
									$size 				= 'foto-menu-col2';
							$image_array 		= wp_get_attachment_image_src($fotografia, $size);
								$image_url 			= $image_array[0];
				?>
				<div class="estado-proyecto-small-menu">
					<?php
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<span class="label label-default"><?php echo $slide_personalizado ;?></span>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
				</div>
				
				<?php if ($pronto):?>
				<a href="<?php echo $pronto;?>" target="_blank" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
					<?php else:?>
					<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
						<?php endif;?>
						<?php if ($fotografia):?>
						<img src="<?php echo $image_url;?>" alt="Proyecto <?php the_title();?>" class="img-responsive img-full" />
						<?php else:?>
						<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" alt="Sin Fotografía" class="img-responsive img-full"/>
						<?php endif;?>
						
						
						<div class="listado_de_proyectos altura-proyectos">
							<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
							<h4><?php the_title(); ?></h4>
						</div>
					</a>
				</div>
				<?php
				endwhile;
				endif;
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>