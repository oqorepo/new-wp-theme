<div class="slide-proyecto-wrap">
	
	<!-- Descripción Textos -->
	<div class="slide-proyectos-descripcion">
		<div class="slide-compactar">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" class="img-responsive"/>
		</div>
		<h2>
		A LA ALTURA DE TUS SUEÑOS
		</h2>
		<div class="tips">
			<li><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/check.svg" class="img-responsive" />
				<p>Ubicación privilegiada con excelente conectividad. Esto es una linea que soporta hasta 3 lineas de textos.</p></li>
				<li><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/check.svg" class="img-responsive" /><p>Plaza interior.</p></li>
				<li><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/check.svg" class="img-responsive" /><p>Estacionamiento de superficie y subterráneos</p></li>
				<li><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/check.svg" class="img-responsive" /><p>Estacionamiento de visitas</p>
			</li>
		</div>
	</div>
	
	<!-- Vistas Previas -->
	<div class="slide-proyectos-vistas vistas-previas">
		<li><img src="http://via.placeholder.com/1198x620/343434/FFFFFF?text=Cocina" class="img-responsive proyectos-vista" /></li>
		<li><img src="http://via.placeholder.com/1198x620/454545/FFFFFF" class="img-responsive proyectos-vista" /></li>
		<li>
			<div class="video-play">
				<img src="http://via.placeholder.com/1198x620/009bdb/FFFFFF" class="img-responsive proyectos-vista" />
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/play.svg" class="play img-responsive"/>
			</div>
		</li>
	</div>
	<!-- Slide Principal -->
	<div class="video-tabs"><iframe width="100%" height="520" src="https://www.youtube.com/embed/rN6nlNC9WQA" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>
	<img src="http://via.placeholder.com/1198x620/343434/FFFFFF?text=Cocina" class="img-responsive imagen-slider actual-slide-proyecto" />
</div>