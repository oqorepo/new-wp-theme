



<!-- Aviso PopUP -->
<?php
$query = new WP_Query(array(
	'post_type'     => array('promociones'),
    'posts_per_page'  => -1,
    'post_status'   => 'publish',
));
if ( $query->have_posts() ) : ?>
<?php while ( $query->have_posts() ) : $query->the_post(); ?>
	<?php $aviso = get_field('activar_aviso_popup');
	$avisoglobal = get_field('¡activar_popup_en_todo_el_sitio!');
	$post_slug=$post->post_name;
	$selecciona = get_field('selecciona_proyecto_o_planta');
	?>


	<?php // Si está activa la opción Global 
	if ($avisoglobal == "Si"):?>




<script type="text/javascript">
$(window).on('load',function(){
	// Cuando carga el sitio, lee la cookie
	var NombreCookie = Cookies.get('AvisoPopUp');
	if (NombreCookie == "<?php the_title();?>"){
	}
	else {
	$('#avisopop').modal('show');
	}
	// Si hace click en el PopUp crea la Cookie
	$('.linkEnPopUp').on('click', function(){
	var nombrePopUp = '<?php the_title();?>';
	Cookies.set('AvisoPopUp', nombrePopUp);
	});

});
</script>

	<?php get_template_part('modulos/avisopop');?>

	<?php // Si está activa la opción por Proyecto
	elseif($aviso == "Si"):?>

	<?php get_template_part('modulos/avisopop');?>


<?php $url_actual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		?>

<?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
<?php $url_proyecto = get_the_permalink();?>




<?php if ($url_proyecto === $url_actual):?>

<script type="text/javascript">
$(window).on('load',function(){
	// Cuando carga el sitio, lee la cookie
	var NombreCookie = Cookies.get('AvisoPopUp');
	if (NombreCookie == "<?php the_title();?>"){
	}
	else {
	$('#avisopop').modal('show');
	}
	// Si hace click en el PopUp crea la Cookie
	$('.linkEnPopUp').on('click', function(){
	var nombrePopUp = '<?php the_title();?>';
	Cookies.set('AvisoPopUp', nombrePopUp);
	});

});
</script>
<?php endif;?>





<?php endforeach; ?>
<?php wp_reset_postdata();?>



	<?php else: endif;?>



<?php endwhile;?>
<?php endif;?>


<!--  -->
