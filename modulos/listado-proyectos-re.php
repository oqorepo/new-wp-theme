<?php
	$_terms = $terms = get_terms( 'ubicaciones', array(
			'hide_empty' => true,
			'parent' => '25',
			) );
foreach ($_terms as $term) :?>
<?php
$nombre = $term->name;
$slug = $term->slug;
?>
<?php
$_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
						'post_status'		=> 'publish',
			'tax_query' => array(
			array(
			'taxonomy' => 'ubicaciones',
			'field'    => 'slug',
			'terms'    => $slug,
			'parent'   => '25',
			),
			),
			));
if( $_posts->have_posts() ) :
$count = $_posts->found_posts;
?>
<?php
			while ( $_posts->have_posts() ) : $_posts->the_post();
		
$tag = get_field('selecciona_tag_del_proyecto');
?>
<script>
$(document).ready(function() {
// Si existen proyectos y todos están agotados bajo la misma ubicación
	
if ($('.proyectos-<?php echo $slug;?>').length === 0){
$('.btn-<?php echo $slug;?>').hide();
}
$('.btn-<?php echo $slug;?>').on('click', function(e){
e.preventDefault();
$('.btn-gris').removeClass('btn-activo');
$(this).addClass('btn-activo');
$('.proyectos_respuesta').hide();
$('.proyectos-<?php echo $slug;?>').show();
});
$('.btn-volver').on('click', function(e){
e.preventDefault();
$('.proyectos_respuesta').hide();
$('.proyectos-<?php echo $slug;?>').hide();
});
});
</script>
<button class="btn btn-lg btn-gris btn-llamado-proyectos btn-<?php echo $slug;?>">
<?php echo $nombre; ?>
<span class="badge hide"><?php echo $count;?></span>
</button>
<?php break;?>
<?php endwhile;?>
<?php endif;?>
<?php endforeach;?>
<div id="lista-proyectos-ubicacion">
	<?php
	$_posts = new WP_Query( array(
				'post_type'         => 'proyectos',
				'posts_per_page'    => -1,
				'tax_query' => array(
				array(
				'taxonomy' => 'ubicaciones',
				'terms'    => 25,
				),
				),
				));
	if( $_posts->have_posts() ) :?>
	<?php
	while ( $_posts->have_posts() ) : $_posts->the_post();
	$pronto = get_field('url_landing_proyecto');
	$tag = get_field('selecciona_tag_del_proyecto');
	?>
	<?php if ($tag === "Agotado"):?>
	<?php else:?>
	<div class="col-md-3 proyectos_respuesta
		<?php
		$terms = get_the_terms( $post->ID , 'ubicaciones' );
		if ( $terms != null ){
		foreach( $terms as $term ) {
		echo 'proyectos-'.$term->slug.'' ;
		unset($term);
		} } ?> animated slideInLeft" style="display:none;">
<div class="estado-proyecto-small-menu">
					<?php
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<span class="label label-default"><?php echo $slide_personalizado ;?></span>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
				</div>
		<?php
			$fotografia = get_field('foto_portada_para_menu');
			$size = 'foto-menu-col3';
			$image_array = wp_get_attachment_image_src($fotografia, $size);
			$image_url = $image_array[0];
		?>
		
		<?php if ($pronto):?>
		<a href="<?php echo $pronto;?>" target="_new" class="tooltipster" title="Ver Proyecto" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
			<?php else:?>
			<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
				<?php endif;?>
				<img src="<?php echo $image_url;?>" class="img-responsive img-full" alt="Proyecto <?php the_title();?>" />
				
				<div class="listado_de_proyectos altura-proyectos-buscador">
					<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
					<h4><?php the_title(); ?></h4>
					<hr></hr>
					<p><?php the_field('bloque_rojo');?></p>
					
					
				</div>
			</a>
		</div>
		<?php endif;?>
		<?php endwhile;?>
		<?php endif;?>
	</div>
	<?php 	wp_reset_postdata();?>