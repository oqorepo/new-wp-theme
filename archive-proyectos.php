<?php
get_header();
?>

<script>
$(document).ready(function() {
$('.head').css('border', '0px');
// Google Tag Manager
dataLayer.push({
'Proyecto': '<?php the_title();?>',
'tipoPagina': 'Proyecto',
});
});
</script>

<?php get_template_part('secciones/header-normal');?>

<?php
$terms = get_the_terms( $post->ID , 'ubicaciones' );
if ( $terms != null ):?>
<?php
foreach( $terms as $term ) :?>
<?php if ($term->parent == "23"):?>
<script>
$('.head').css('border-bottom', '0px');
</script>
<?php elseif ($term->parent == "25"):?>
<script>
$('.head').css('border-bottom', '0px');
</script>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>






<!-- ============================================================================= -->
<!-- Sección Título -->
<!-- ============================================================================= -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">Proyectos</h1>
	</div>
</section>


<!-- ============================================================================= -->
<!-- Sección Proyectos -->
<!-- ============================================================================= -->


<div class="separador-section"></div>


<div class="separador-de-seccion">	
<div class="container">
	<div class="row">
		<h2 class="titulos">Proyectos en Santiago</h2>
		<?php
		$_posts = new WP_Query( array(
		'post_type'         => 'proyectos',
		'posts_per_page'    => -1,
			'tax_query' => array(
			array(
			'taxonomy' => 'ubicaciones',
			'terms'    => '23'),
			),
		));
		if( $_posts->have_posts() ) :
		while ( $_posts->have_posts() ) : $_posts->the_post();
		$pronto = get_field('url_landing_proyecto');
		$fotografia_menu = get_field('foto_portada_para_menu');
		$size = 'foto-menu-col4';
		$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
		?>
		
		<div class="col-md-3 bloque-proyecto-menu">
			
			<?php
			$fotografia = get_field('foto_portada_para_menu');
				$size = 'foto-menu-col4';
				$image_array = wp_get_attachment_image_src($fotografia, $size);
				
			?>
			<div class="estado-proyecto-small-menu">
				<?php
				// variables
				$tag = get_field('selecciona_tag_del_proyecto');
				$tag_personalizado = get_field('personalizar_tag');
				if ($tag == "Personalizar"):?>
				<span class="label label-default"><?php echo $tag_personalizado;?></span>
				<?php elseif ($tag == "Normal"):?>
				<?php else:?>
				<span class="label label-default"><?php echo $tag;?></span>
				<?php endif;?>
			</div>
			
			<?php if ($pronto):?>
				<a href="<?php echo $pronto;?>" target="_blank" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
				<?php else:?>
				<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
					<?php endif;?>
					<?php if ($fotografia):?>
					<img src="<?php echo $image_array[0];?>" class="img-responsive img-full" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full"/>
					<?php endif;?>
					
					
				<div class="listado_de_proyectos altura-proyectos-buscador">
				<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
				<h4><?php the_title(); ?></h4>
				<hr></hr>
				<p><?php the_field('bloque_rojo');?></p>
				
				
			</div>
				</a>
			</div>
			<?php
			endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</div>


</div>


<div class="separador-de-seccion">	
<div class="container">
	<div class="row">
		<h2 class="titulos">Proyectos en Regiones</h2>
		<?php
		$_posts = new WP_Query( array(
		'post_type'         => 'proyectos',
		'posts_per_page'    => -1,
			'tax_query' => array(
			array(
			'taxonomy' => 'ubicaciones',
			'terms'    => '25'),
			),
		));
		if( $_posts->have_posts() ) :
		while ( $_posts->have_posts() ) : $_posts->the_post();
		$pronto = get_field('url_landing_proyecto');
		$fotografia_menu = get_field('foto_portada_para_menu');
		$size = 'foto-menu-col3';
		$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
		?>
		
		<div class="col-md-3 bloque-proyecto-menu">
			
			<?php
			$fotografia = get_field('foto_portada_para_menu');
				$size = 'foto-menu-col3';
				$image_array = wp_get_attachment_image_src($fotografia, $size);
				$image_url = $image_array[0];
			?>
			<div class="estado-proyecto-small-menu">
				<?php
				// variables
				$tag = get_field('selecciona_tag_del_proyecto');
				$tag_personalizado = get_field('personalizar_tag');
				if ($tag == "Personalizar"):?>
				<span class="label label-default"><?php echo $tag_personalizado;?></span>
				<?php elseif ($tag == "Normal"):?>
				<?php else:?>
				<span class="label label-default"><?php echo $tag;?></span>
				<?php endif;?>
			</div>
			
			<?php if ($pronto):?>
			<a href="<?php echo $pronto;?>" target="_blank" class="tooltipster2" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
				<?php else:?>
				<a href="<?php the_permalink();?>" class="tooltipster2" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
					<?php endif;?>
					<?php if ($fotografia):?>
					<img src="<?php echo $image_url;?>" class="img-responsive img-full" />
					<?php else:?>
					<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full"/>
					<?php endif;?>
					
					
				<div class="listado_de_proyectos altura-proyectos-buscador">
				<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
				<h3><?php the_title(); ?></h3>
				<hr></hr>
				<p><?php the_field('bloque_rojo');?></p>
				
				
			</div>
				</a>
			</div>
			<?php
			endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</div>
</div>
</div>
</div>

<?php get_footer();?>
