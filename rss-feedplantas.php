<?php
/**
 * Template Name: Feedplantas
 */

$idPlanta = $_GET['idplanta'];
if ($idPlanta):
$query = new WP_Query(array(
                'post_type'             => 'plantas',
                'posts_per_page'        => 1,
                'post_status'           => 'publish',
                'post__in'				=> $idPlanta

));
endif;
if ( $query->have_posts() ) :
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        <?php do_action('rss2_ns'); ?>>
<channel>
        <title><?php bloginfo_rss('name'); ?> - Feed</title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description') ?></description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
        <?php do_action('rss2_head'); ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <?php 
        $estado = get_field('estado');
        $fotografia_planta = get_field('fotografia_planta');
        $fotografia_planta_array = wp_get_attachment_image_src($fotografia_planta, 'full');
        ?>
                <item>
                        <title><?php the_title(); ?></title>
                        <link><?php the_permalink_rss(); ?></link>
                        <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                        <guid isPermaLink="false"><?php the_guid(); ?></guid>
                        <description><?php the_field('bloque_rojo');?></description>
                        <content:encoded><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?></content:encoded>

                        <image>
                                <title><?php the_title();?></title>
                                <url><?php echo $fotografia_planta_array[0];?></url>
                                <link><?php the_permalink_rss(); ?></link>
                                <description><?php echo the_field('descripcion_breve_pagina_del_proyecto');?> </description>
                        </image>
           

                        <category>
                        <?php echo $estado;?>
                        </category>

                        <?php rss_enclosure(); ?>
                        <?php do_action('rss2_item'); ?>



                </item>
        <?php endwhile; ?>
</channel>
</rss>
<?php endif;?>
