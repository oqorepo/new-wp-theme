<?php
/**
 * Template Name: Página Contacto
*/
get_header(); 
get_template_part('secciones/header-normal');
?>




<section class="pagina-interior">

<div class="container">





<div class="col-md-4">
	<h1 class="display text-left"><?php the_title();?></h1>
<h2 class="titulos text-left">¡En Avellaneda nos interesa tu opinión!</h2>
<p>
¿Tienes alguna duda, comentario o sugerencia? comunícate con nosotros a través del siguiente formulario y te contactaremos a la brevedad.</p>
</div>


<div id="gracias-form-contacto">

	<div class="col-md-8">
	<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado.svg" class="img-responsive icon-enviado">
	<h3>Gracias <span class="nombre-persona"></span></h3>
	<p><strong>Tu mensaje ha sido enviado con éxito.</strong></p>
	<p>Pronto te contactaremos.</p>
	</div>
</div>


<div id="formulario-contacto">
<div class="col-md-8">
<?php echo do_shortcode('[contact-form-7 id="626" title="Contacto" html_id="formulario-contacto"]');?>
</div>
</div>




</div>

</section>


<?php get_footer();?>
