
<?php
if (! is_user_logged_in() ) {
	auth_redirect();
}
?>

<header>
	<div class="head">
		<div class="col-md-6">
			<div class="logo">
				<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="Avellaneda"></a>
			</div>
			
		</div>

		<div class="col-md-6 pull-right">



	<div class="pull-right" style="margin-top:15px;"><a href="<?php echo wp_logout_url( get_permalink() ); ?>"><button class="btn btn-rojo"><i class="fas fa-times-circle"></i> Salir</button></a></div>




	<div class="pull-right" style="margin-top:15px; margin-right:10px;"><a href="<?php bloginfo('url');?>/privado/registros"><button class="btn btn-gris-claro"><i class="fas fa-chevron-left"></i> Volver a Registros</button></a></div>


			<div class="pull-right" style="margin-top:15px; margin-right:10px;">
<?php global $current_user; wp_get_current_user(); ?>
<?php if ( is_user_logged_in() ) { 
echo '<button class="btn btn-gris"><i class="fas fa-user"></i> Hola ' . $current_user->display_name . "\n</button> "; } 
else { 

 } ?>
</div>




	
		</div>
		
	</div>
</header>




