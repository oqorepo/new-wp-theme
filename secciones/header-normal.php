<script>
		$(document).ready(function() {
	    
	    $('.head').css('border', '0px');

	    if ($('ul.menu-principal').find('.current-menu-ancestor').length) {
	    	    $('.linea-menu-bot').css('background', '#e5302d');
	    }

	        if ($('ul#menu-principal').find('.current_page_item').length) {
	    	    $('.linea-menu-bot').css('background', '#e5302d');
	    }

	});
	</script>

<header>
	<?php get_template_part('secciones/menu-top');?>
	<div class="head">
		<div class="container">
			<div class="logo animated fadeInLeft">
				<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="<?php the_title();?>"></a>
			</div>
			<ul class="menu-principal">
				<li class="menu_proyectos_rm"><a href="#">Proyectos región metropolitana</a></li>
				<li class="menu_proyectos_re"><a href="#">Proyectos Regiones</a></li>
		<?php get_template_part('secciones/menu-wp');?>
			</ul>
		</div>
		<?php get_template_part('modulos/menu_rm');?>
		<?php get_template_part('modulos/menu_re');?>
	
			<div class="linea-menu-bot"></div>
	</div>


</header>





<header class="celular">
	<div class="container">
		<div class="logo">
			<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" alt="<?php the_title();?>" class="img-responsive"></a>
		</div>



<div class="mh-head Sticky">
	<span class="mh-btns-left">
		<a href="#menu"> <i class="fas fa-bars"></i></a>
		<!-- <a class="mh-hamburger" href="#menu"></a> -->
	</span>
	
</div>
	
<div class="buscador">
	<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/buscar.svg" alt="Buscar" class="img-responsive">
</div>



	</div>
	<?php get_template_part('secciones/menu-top-celular');?>
</header>

<?php get_template_part('secciones/menu-celular');?>





<div class="menu-fixed">

	<?php if( is_page( 'proyecto' )):?>

	<div class="menu-cotizar">
		<div class="botones-menu-fixed">
			<div class="iconos-menu-fixed">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/contacto.svg" class="img-responsive" alt="Contacto" />
			</div>
			<p><strong>Solicitar</strong> Información</p>
		</div>
	</div>
<?php endif;?>


	<div class="container">
		<div class="logo">
			<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="<?php bloginfo('name');?>"></a>
		</div>
		<ul class="menu-principal menu-proyectos-fix">
		<li class="menu_proyectos_rm"><a href="#">Proyectos región metropolitana</a></li>
		<li class="menu_proyectos_re"><a href="#">Proyectos Regiones</a></li>
		<?php get_template_part('secciones/menu-wp');?>

		</ul>







	</div>











	<div class="logos-redes-top">


<div class="social-networks">
	<a href="https://www.youtube.com/user/AvellanedaVideos" title="Síguenos en YouTube" target="_blank" class="social-networks__youtube tooltipster2" data-tooltipster='{"side":"bottom"}'>
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/youtube.svg" class="img-responsive" alt="Youtube" />
	</a>
	<a href="https://www.instagram.com/inmobiliariaavellaneda/" title="Síguenos en Instagram" target="_blank" class="social-networks__instagram tooltipster2" data-tooltipster='{"side":"bottom"}'>
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/instagram.svg" class="img-responsive" alt="Instagram" />
	</a>
	<a href="https://www.facebook.com/InmobiliariaAvellaneda" title="Síguenos en Facebook" class="social-networks__facebook tooltipster2" target="_blank" data-tooltipster='{"side":"bottom"}'>
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/facebook.svg" class="img-responsive" alt="Facebook" />
	</a>
</div>




		</div>




<?php get_template_part('modulos/menu_rm');?>
<?php get_template_part('modulos/menu_re');?>

	<div class="linea-menu-bot"></div>

</div>
	<?php wp_reset_postdata(); ?>