<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="descargarpdf" tabindex="999999" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close tooltipster" title="Cerrar" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">FOLLETO DIGITAL <?php the_title();?></h4>
      </div>
      <div class="modal-body">
        


<script>
$(document).ready(function() {
nombre_proyecto = "<?php the_title();?>";
Cookies.set('Nombre_Proyecto', nombre_proyecto);
var formulario_descarga_pdf_enviado = Cookies.get('form_pdf_para_'+nombre_proyecto+'');
if (formulario_descarga_pdf_enviado === 'Enviado - <?php the_title();?>' ) {
$('#formulario-puede-descargar').fadeIn();
$('#formulario-descargar').hide();
} else {
}
});
 </script>

<div id="formulario-descargar">
  <p class="small">Para descargar el PDF del proyecto <?php the_title();?> completa los campos</p>
  <hr></hr>
  <?php echo do_shortcode('[contact-form-7 id="974" title="Descargar PDF" html_id="formulario-descargar-pdf"]');?>
</div>

<div id="formulario-puede-descargar">
  <?php $archivo = get_field('sube_el_ebook_digital');?>
  <a href="<?php echo $archivo;?>" download><button class="btn btn-rojo btn-block"><i class="fas fa-file-pdf"></i> Descargar Folleto Digital</button></a>
</div>



          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-default btn-rojo" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
            
            
          </div>
        </div>
      </div>
    </div>
