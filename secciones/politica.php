<!-- Modal -->
<div class="modal fade" id="politica" tabindex="999999" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close tooltipster" title="Cerrar" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">POLÍTICA DE PRIVACIDAD</h4>
      </div>
      <div class="modal-body">
        <p>"Las imágenes contenidas en este sitio son elaborados con fines ilustrativos y no constituyen una representación exacta de la realidad, en consecuencia no forman parte de las condiciones ofrecidas al comprador. Precios finales deben ser corroborados en sala de ventas de acuerdo a las unidades existentes en el momento."</p></br>
        <p> Los datos de carácter personal, según lo prescrito por la Ley N° 19.628, entregados por los usuarios del sitio web
          <strong>www.avellaneda.cl</strong>, serán administrados exclusivamente por personal de la empresa, evitando usos indebidos,
        alteración o entrega a terceras personas.</p></br>
        <p>
          <strong>Avellaneda</strong> no se hará responsable del uso que puedan dar terceras personas a los datos de carácter personal entregados por sus titulares en foros, redes sociales, espacios abiertos al público o a través de cualquier otro medio, ni del uso que de dichos datos puedan hacer terceros que los hayan obtenido de forma maliciosa del sitio web.<p></br>
            <p>Las opiniones vertidas en el sitio web <strong>www.avellaneda.cl</strong> son de exclusiva responsabilidad de quienes las emiten.</p></br>
          </div>
          <div class="modal-footer">
            <?php if (!is_page('contacto')):?><a href="<?php bloginfo('url');?>/contacto"><div class="btn btn-gris"><i class="fas fa-envelope"></i> Contacto</div></a><?php endif;?>
            <button type="button" class="btn btn-default btn-rojo" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
            
            
          </div>
        </div>
      </div>
    </div>
