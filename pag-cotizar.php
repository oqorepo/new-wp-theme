<?php
/**
 * Template Name: Cotizar Proyectos
*/
get_header(); 
get_template_part('secciones/header-normal');
?>


<?php
if (get_query_var('proyecto')):
$the_slug = get_query_var('proyecto');?>

<?php
		$query = new WP_Query(array(
		'name'        		=> $the_slug,
		'post_type'      	=> 'proyectos',
		'posts_per_page'	=> 1,
		'post_status'		=> 'publish',
		));
?>
<?php if ( $query->have_posts() ) : ?>



<?php while ( $query->have_posts() ) : $query->the_post();
$titulo = get_the_title();
$fotografia = get_field('foto_portada_para_menu');
$size = 'full';
$image_array = wp_get_attachment_image_src($fotografia, $size);
$image_url = $image_array[0];
$bloque_rojo = get_field('bloque_rojo');
$pronto = get_field('url_landing_proyecto');
// Variable si el proyecto está subido re-envia URL
$permalink = 'www.google.cl';
if ($pronto):?>
<?php else:?>

<?php wp_redirect( get_permalink(  ) ); exit; ?>
<?php endif;?>





<script>

 $(document).ready(function() {
var proyecto = $('.nombre-proyecto').text();
$('input[name="Proyecto"]').val(proyecto);
console.log(proyecto);

});


</script>

<section class="pagina-interior">
	
	<div class="container">

		<h2 class="title">Cotizar <span class="nombre-proyecto"><?php echo $titulo;?></span></h2>




<?php if ($fotografia):?>
<div class="col-md-6">


	<div class="estado-proyecto-small-menu">
					<?php
					// variables
					$tag = get_field('selecciona_tag_del_proyecto');
					$tag_personalizado = get_field('personalizar_tag');
					if ($tag == "Personalizar"):?>
					<span class="label label-default lb-lg"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>

					<?php elseif ($tag == "Agotado"):?>




					<?php else:?>
					<span class="label label-default lb-lg"><?php echo $tag;?></span>
					<?php endif;?>
					
				</div>



<?php if ($tag === "Agotado"):?>
  <div class="ribbon ribbon-top-left" style="left:15px;"><span><?php echo $tag;?></span></div>
<?php endif;?>

<img src="<?php echo $image_url;?>" class="img-responsive">


<?php if ($tag === "Agotado"):?>

<?php else:?>
</br>
<strong>UBICACIÓN</strong>
<span style="text-transform:uppercase;"><?php get_template_part('modulos/ubicacion-actual');?></span>
</p>
<hr></hr>
<div class="row">
<div class="col-md-12">
	<p>
		<?php echo $bloque_rojo;?>
	</p>
</div>
</div>
<?php endif;?>




</div>



<?php endif;?>


<div id="gracias-form-cotizar">

	<div class="col-md-6">
	<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado2.svg" class="img-responsive icon-enviado">
	<h3>Gracias <span class="nombre-persona"></span></h3>
	<p><strong>Tu cotización ha sido enviada con éxito.</strong></p>
	<p>Pronto te contactaremos.</p>
	</div>
</div>



<div id="formulario-cotizar">
<div class="col-md-6">
<?php if ($tag === "Agotado"):?>

</br>
<strong>UBICACIÓN</strong>
<span style="text-transform:uppercase;"><?php get_template_part('modulos/ubicacion-actual');?></span>
</p>
<hr></hr>
<div class="row">
<div class="col-md-12">
	<p>
		<?php echo $bloque_rojo;?>
	</p>
</div>
</div>


<?php else:?>

<?php echo do_shortcode('[contact-form-7 id="505" title="Cotización de Proyecto" html_id="formulario-cotizar"]');?>

<?php endif;?>

</div>
</div>






</div>
</section>

<?php endwhile;?>

<?php endif;?>



<?php else: // No hay proyectos ?>

<?php endif;?>


<?php get_footer();?>

