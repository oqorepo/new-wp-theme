<?php
/**
 * Template Name: Feedproyectos
 */
$idProyecto = $_GET['idproyecto'];
if ($idProyecto):
$query = new WP_Query(array(
                'post_type'             => array('proyectos'),
                'posts_per_page'        => -1,
                'p'                     => $idProyecto,
                'post_status'           => 'publish',
));
else:
$query = new WP_Query(array(
                'post_type'             => array('proyectos'),
                'posts_per_page'        => -1,
                'post_status'           => 'publish',
));
endif;
if ( $query->have_posts() ) :
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        xmlns:media="http://search.yahoo.com/mrss/"
        <?php do_action('rss2_ns'); ?>>
<channel>
        <title><?php bloginfo_rss('name'); ?> - Feed</title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss('description') ?></description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
        <?php do_action('rss2_head'); ?>

        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <item>
                        <title><?php the_title(); ?></title>
                        <link><?php the_permalink_rss(); ?></link>
                        <guid isPermaLink="false"><?php the_guid(); ?></guid>
                        <description><?php the_field('cuerpo_del_mensaje');?></description>
                        <content:encoded><?php the_field('cuerpo_del_mensaje');?></content:encoded>

                        
                        <?php $imagen = get_field('imagen_destacada');?>
                        <?php if ($imagen):?>
                         <media:content url="<?php echo $imagen;?>" type="image/jpg" />
                        <?php else:?>
                        <?php if( have_rows('slides_principal') ):
                                while ( have_rows('slides_principal') ) : the_row();
                                $fotografia_mobile      = get_sub_field('fotografia_mobile');
                                $image_array_mobile = wp_get_attachment_image_src($fotografia_mobile, $size);
                                ?>
                        <media:content url="<?php echo $image_array_mobile[0];?>" type="image/jpg" />

                       
                        <?php break;?>
                        <?php endwhile;?>               
                        <?php endif;?>
                        <?php endif;?>

                        <category>
                                <?php
                                $tag = get_field('selecciona_tag_del_proyecto');
                                if ($tag):?>
                                <?php
                                $tag_personalizado = get_field('personalizar_tag');
                                if ($tag == "Personalizar"):?>
                                <?php echo $tag_personalizado;?>
                                <?php elseif ($tag == "Normal"):?>
                                <?php else:?>
                                <?php echo $tag;?>
                                <?php endif;?>
                                <?php endif;?>
                        </category>

                        <?php rss_enclosure(); ?>
                        <?php do_action('rss2_item'); ?>






                </item>
        <?php endwhile; ?>
</channel>
</rss>
<?php endif;?>
